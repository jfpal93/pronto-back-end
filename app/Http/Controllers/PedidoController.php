<?php

namespace App\Http\Controllers;

use App\Orden;
use App\Pedido;
use App\PedidoDetalle;
use Illuminate\Http\Request;
use App\Producto;
use App\User;

use DB;
use Response;
use App\RepartidorEstadoUser;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use App\FirebaseToken;


class PedidoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function getPedidosEstablecimiento($estabId){
        try {
            $myArray = []; 

            foreach(Pedido::where('establecimiento_id', $estabId)->get() as $ped)
            {
                $user = DB::table('ordens')
                ->join('pedidos', 'ordens.id', 'pedidos.orden_id')
                ->join('users', 'users.id', 'ordens.user_id')
                ->select( 
                    'ordens.codigo' 
                    )
                ->where('pedidos.id', $ped->id)
                ->get();

                $myArray2 = []; 
                $products = PedidoDetalle::where('pedido_id', $ped->id)->get();

                foreach($products as $p){
                    $prod=Producto::where('id',$p->producto_id)->first();
                    $arrayProd=array(
                        'product'=>$prod,
                        'cant'=>$p->cant);
                    array_push($myArray2, $arrayProd);
                }

                $pedTemp=array(
                    'products'=>$myArray2,
                    'total'=>$ped->total,
                    'subtotal'=>$ped->total,
                    'estado'=>$ped->estado,
                    'tiempo'=>$ped->tiempo,
                    'establecimiento_id'=>$ped->establecimiento_id,
                    'orden_id'=>$ped->orden_id,
                    'user'=>$user,
                    'codigo'=>$user[0]->codigo
                );

                array_push($myArray, $pedTemp);

            }

            return $myArray;
        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function selectCookTimeEstab(Request $request,$estabId){
        try {

            //Actualizacion de pedido

            $pedido=Pedido::where('establecimiento_id', $estabId)
            ->where('orden_id', $request->ordenId)
            ->first();

            $pedido->estado="TS";

            // return $pedido->update();;

            $pedido->tiempo=$request->time;

            //Tiempo seleccionado
            $pedido->estado="TS";

            $pedido->update();

            //Actualizacion de orden

            $orden=Orden::where('id',$request->ordenId)->first();

            $orden->tiempoEstabs=$orden->tiempoEstabs+$request->time;
            $orden->update();

            $pedidos=Pedido::where('orden_id', $request->ordenId)
            ->get();

            if($pedidos->count())
            {
                ///Contedo de pedidos
                $pedCount=$pedidos->count();

                $pedCountTemp=0;

                //Verificacion de pedidos con tiempo seleccionado
                foreach($pedidos as $ped) {

                    if($ped->estado=="TS"){
                        $pedCountTemp=$pedCountTemp+1;
                    }

                }

                if($pedCount == $pedCountTemp){
                    //Tiempo seleccionado
                    $orden->estado="TS";
                    $orden->update();

                    // $user=User::where('id',1)->first();

                    $users = DB::table('users')
                    ->join('model_has_roles', 'users.id', 'model_has_roles.model_id')
                    ->select( 
                        'users.id as id'
                        )
                    ->where('model_has_roles.role_id', 1)
                    ->orWhere('model_has_roles.role_id', 5)
                    ->pluck('id')->toArray();

                    $optionBuilder = new OptionsBuilder();
                    $optionBuilder->setTimeToLive(60*20);
        
                    $notificationBuilder = new PayloadNotificationBuilder('¡Nueva Orden!');
                    $notificationBuilder->setBody('Asignar tiempo, costo y repartidor de delivery')
                                        ->setSound('default');
        
                    $dataBuilder = new PayloadDataBuilder();
                    $dataBuilder->addData(['a_data' => 'my_data']);
        
                    $option = $optionBuilder->build();
                    $notification = $notificationBuilder->build();
                    $data = $dataBuilder->build();

                    $userTokens=FirebaseToken::whereIn('user_id',$users)->pluck('firebase_token')->toArray();

        
                    // echo $user->firebase_token;
                    if(count($userTokens)>0){
        
                        // $token = $user->firebase_token;
            
                        $downstreamResponse = FCM::sendTo($userTokens, $option, $notification, $data);
                        $downstreamResponse->numberSuccess();
                        $downstreamResponse->numberFailure();
                        $downstreamResponse->numberModification();
                    }
                }
            }

        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }

    }

    public function setAvailableRepartidor($userId){
        try {
            $rep=RepartidorEstadoUser::where('user_id',$userId)->first();
            $rep->estado_repartidor_id=1;
            $rep->update();
        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }

    }

    public function setUnavailableRepartidor($userId){
        try {
            $pedidos=Orden::where('repartidor_id', $userId)
            ->where('ordens.estado','DRCA')
            ->orWhere('ordens.estado','DR')
            ->orWhere('ordens.estado','DA')->get();

            if($pedidos->count())
            {
                return null;
            }
            else{
                $rep=RepartidorEstadoUser::where('user_id',$userId)->first();
                $rep->estado_repartidor_id=2;
                $rep->update();

                return response()->json(['state' => 'success']);
            }
            
        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
