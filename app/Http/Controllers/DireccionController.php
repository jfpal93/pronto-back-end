<?php

namespace App\Http\Controllers;

use App\Direccion;
use Illuminate\Http\Request;
use DB;

class DireccionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

   public function getDireccion($userId){
        try {
            $direcciones = DB::table('direccions')
                ->where('direccions.user_id', $userId)
                ->orderBy('selected','desc')->latest()
                ->get();
            return $direcciones;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
   }

    public function addDirection(Request $request, $userId){
        //Request Data
        //nombre
        //direccion
        //lat
        //lng

        try {

            $nombre=$request->nombre;
            $direccion=$request->direccion;
            $lat=$request->lat;
            $lng=$request->lng;

            $dir= new Direccion;
            $dir->nombre=$nombre;
            $dir->direccion=$direccion;
            $dir->lat=$lat;
            $dir->lng=$lng;
            $dir->user_id=$userId;
            $dir->selected=false;
            $dir->save();

            return Direccion::where('user_id' , $userId)
            ->orderBy('selected','desc')->latest()->get();

        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function editDirection(Request $request, $userId){
        //Request Data
        //nombre
        //direccion
        //lat
        //lng
        //id

        try {

            $nombre=$request->nombre;
            $direccion=$request->direccion;
            $lat=$request->lat;
            $lng=$request->lng;

            $dir = Direccion::find($request->id);

            $dir->nombre=$nombre;
            $dir->direccion=$direccion;
            $dir->lat=$lat;
            $dir->lng=$lng;
            $dir->user_id=$userId;
            $dir->update();

            return Direccion::where('user_id' , $userId)
            ->orderBy('selected','desc')->latest()->get();

        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function deleteDirection(Request $request, $userId){
        //Request Data
        //nombre
        //direccion
        //lat
        //lng
        //id

        try {
            $dir = Direccion::find($request->id);
            // $dir = Direccion::destroy($request->id);
            $dir->delete();

            return Direccion::where('user_id' , $userId)
            ->orderBy('selected','desc')->latest()->get();


        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function selectDirection(Request $request, $userId){
        //Request Data
        //dirId

        try {


            $dir = Direccion::find($request->dirId);

            $dir->selected=true;
            $dir->update();

            $dir2 = Direccion::where('user_id' , $userId)
            ->where('id','!=' , $request->dirId)->get();
            // Log::useDailyFiles(storage_path().'/logs/err.log');
            // Log::info($dir2);
            foreach ($dir2 as $d) {
                $d->selected=false;
                $d->update();
            }

            return Direccion::where('user_id' , $userId)
            ->orderBy('selected','desc')->latest()->get();
        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function deselectDirection(Request $request, $userId){
        //Request Data
        //dirId

        try {


            $dir = Direccion::find($request->dirId);

            $dir->selected=false;
            $dir->update();

            return Direccion::where('user_id' , $userId)
            ->orderBy('selected','desc')->latest()->get();

        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

}
