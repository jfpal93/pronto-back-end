<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'middleware' => 'api',
], function ($router) {
    Route::get('/clear-cache', function() {
        Artisan::call('cache:clear');
        return "Cache is cleared";
    });

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::post('register', 'AuthController@register');
    Route::post('logoutRepartidor', 'AuthController@logoutRepartidor');
    Route::resource('roles','RoleController');
    Route::resource('categorias','CategoriaController');
    Route::resource('establecimientos','EstablecimientoController');
    Route::resource('favoritos','FavoritoController');
    Route::resource('productos','ProductoController');  

    Route::resource('publicidads','PublicidadController');
    Route::post('carrito/{userId}/placeOrder', 'CarritoController@placeOrder');
    Route::post('carrito/{userId}/add', 'CarritoController@addProductCarritoUsuario');
    Route::post('carrito/{userId}/delete', 'CarritoController@deleteProductCarritoUsuario');
    Route::get('carrito/{userId}', 'CarritoController@getCarritoUsuario');
    Route::resource('sugerencias','SugerenciaController');
    Route::get('users', 'UsersController@index');
    Route::get('clients', 'UsersController@getClients');
    Route::get('repartidores', 'UsersController@getRepartidores');
    Route::put('users/{id}', 'UsersController@update');
    Route::delete('users/{id}', 'UsersController@destroy');
    Route::post('user/{usrId}/token', 'UsersController@updateToken');
    Route::get('establecimiento/{idEstablecimiento}/productos', 'ProductoController@productosEstablecimiento');
    Route::post('direccion/{usrId}/add', 'DireccionController@addDirection');

    Route::get('direccion/{usrId}', 'DireccionController@getDireccion');

    Route::post('direccion/{usrId}/edit', 'DireccionController@editDirection');
    Route::post('direccion/{usrId}/delete', 'DireccionController@deleteDirection');
    Route::post('direccion/{usrId}/select', 'DireccionController@selectDirection');
    Route::post('direccion/{usrId}/deselect', 'DireccionController@deselectDirection');

    Route::get('search', 'SearchController@search');
    Route::post('search/save', 'SearchController@saveSearch');
    Route::get('search/top10', 'SearchController@getTop10');
    Route::get('search/getTop10ByUser', 'SearchController@getTop10ByUser');

    Route::get('getAllData', 'DataController@getAllData');
    Route::get('getAllItems', 'DataController@getAllItems');

    Route::get('getOrdenes/{usrId}', 'OrdenController@getOrdenes');
    Route::get('getOrdenesDR/{usrId}', 'OrdenController@getOrdenesDR');

    Route::get('getOrdenesTS', 'OrdenController@getOrdenesTS');
    Route::get('getOrdenesByRepartidor/{repId}', 'OrdenController@getOrdenesByRepartidor');
    Route::get('getOrdenesCompletasByRepartidor/{repId}', 'OrdenController@getOrdenesCompletasByRepartidor');
    Route::post('setTimePriceDelOrder', 'OrdenController@setTimePriceDelOrder');
    Route::post('setRepOnly', 'OrdenController@setRepOnly');
    Route::post('deliveryAccepted', 'OrdenController@deliveryAccepted');
    Route::post('deliveryCanceled', 'OrdenController@deliveryCanceled');
    Route::post('deliveryArrived', 'OrdenController@deliveryArrived');
    Route::post('deliveryDone', 'OrdenController@deliveryDone');


    Route::get('getPedidosEstab/{estabId}', 'PedidoController@getPedidosEstablecimiento');


    Route::post('selectCookTime/{estabId}', 'PedidoController@selectCookTimeEstab');

    Route::post('setAvailableRep/{userId}', 'PedidoController@setAvailableRepartidor');
    Route::post('setUnavailableRep/{userId}', 'PedidoController@setUnavailableRepartidor');

    Route::get('geofence', 'GeoFenceController@getAllCoordinates');
    Route::post('geofence/edit', 'GeoFenceController@getAllCoordinates');

    // Route::prefix('user')->group(function () { // 'user' can be anything as prefix
    //     Route::get('getAllData', 'DataController@getAllData');
    // });
});

// Route::middleware('auth:api')->get('getAllData', 'DataController@getAllData');

// Route::get('getAllData', 'DataController@getAllData');

// Route::prefix('user')->group(function () { // 'user' can be anything as prefix
//     Route::get('getAllData', 'DataController@getAllData');
// });

