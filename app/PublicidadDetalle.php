<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublicidadDetalle extends Model
{
    protected $fillable = [];

    public function publicidad()
	{
		return $this->belongsTo('App\Publicidad');
	}
}
