<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;

class GeoFenceController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function getAllCoordinates(){
        $coords=DB::table('geofence')->get();
        return Response::json(array('coords'=>$coords));
    }

    public function editCoordinate(Request $request){
        $coords=DB::table('geofence')
        ->where('lat', $request->lat)
        ->where('lng', $request->lng)
        ->update(
            array('lat',$request->newLat,
            'lng',$request->newLng)
        );
    }
}
