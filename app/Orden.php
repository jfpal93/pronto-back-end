<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orden extends Model
{
    protected $table = 'ordens';
    protected $fillable = ['costo_delivery', 'estado', 'tiempo', 'subtotal', 'total',
                            'establecimiento_id', 'user_id', 'direccion_id','cant'];

    public function user()
	{
		return $this->belongsTo('App\User');
    }

    public function repartidor()
	{
		return $this->belongsTo('App\User');
    }

    public function direccion()
	{
		return $this->belongsTo('App\Direccion');
    }

    public function establecimiento()
	{
		return $this->belongsTo('App\Establecimiento');
    }

    public function pedido_detalle() {
        return $this->hasMany('App\PedidoDetalle');
    }
}
