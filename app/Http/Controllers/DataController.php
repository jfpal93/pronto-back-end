<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Favorito;
use App\FavoritoDetalle;
use App\Establecimiento;
use App\Categoria;
use App\Producto;
use App\Publicidad;
use App\PublicidadDetalle;

use Exception;
use Response;
use DB;

class DataController extends Controller
{
    //
    public function getAllData(){

        try {
            $favoritos = Favorito::with('detalle')->get()->shuffle();
            $establecimientos = Establecimiento::all()->shuffle();
            $categorias = Categoria::all()->shuffle();
            $productos = Producto::where('disponible',1)->where('deleted',0)->get()->shuffle();
            $publicidads = Publicidad::with('detalle')->get()->shuffle();
            $search=DB::table('search')
            ->select('search.palabra',DB::raw('COUNT(palabra) as count'))
            ->groupBy('palabra')
            ->orderBy('count', 'DESC')
            ->take(10)
            ->get();
            $coords=DB::table('geofence')->get();

            return Response::json(array(
                'favoritos'=>$favoritos,
                'establecimientos'=>$establecimientos,
                'categorias'=>$categorias,
                'productos'=>$productos,
                'publicidads'=>$publicidads,
                'top10' => $search,
                'coordenadas' => $coords));
            // $user = 'Who am I';
            // return response()->json(['success' => $user], 200);
            
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }

    }

    public function getAllItems(){

        try {
            $productos = Producto::all();

            return Response::json(
                array(
                'items'=>$productos
            ))->header('Access-Control-Allow-Origin', 'https://amp.gmail.dev')
            ->header('AMP-Access-Control-Allow-Source-Origin', 'amp@gmail.dev')
            ->header('Access-Control-Expose-Headers', 'AMP-Access-Control-Allow-Source-Origin');
            
            // ->withHeaders([
            //     'Access-Control-Allow-Origin' =>'https://amp.gmail.dev',
            //     'AMP-Access-Control-Allow-Source-Origin' => 'amp@gmail.dev',
            //     'Access-Control-Expose-Headers' => 'AMP-Access-Control-Allow-Source-Origin',
            // ]);;
            // $user = 'Who am I';
            // return response()->json(['success' => $user], 200);
            
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }

    }
}
