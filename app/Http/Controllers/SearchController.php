<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;
use App\Establecimiento;
use App\Categoria;
use App\Search;
use DB;
use Response;

class SearchController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['search','saveSearch','getTop10']]);
    }

    public function search(Request $request)
    {
        $search=$request->search;

        $products =  DB::table('productos')
        ->join('establecimientos', 'productos.establecimiento_id', 'establecimientos.id')
        ->join('categorias', 'categorias.id', 'establecimientos.categoria_id')
        ->select(
        'productos.id',
        'productos.nombre',
        'productos.linkImagen',
        'productos.descripcion',
        'productos.precio',
        'productos.disponible',
        'categorias.id AS categoria_id',
        'categorias.nombre AS categoria',
        'productos.establecimiento_id', 
        'establecimientos.nombre AS establecimiento')
        ->where('productos.nombre', 'like', "%{$search}%")
        ->orWhere('productos.descripcion', 'like', "%{$search}%")
        ->orWhere('categorias.nombre', 'like', "%{$search}%")
        ->orWhere('establecimientos.nombre', 'like', "%{$search}%")
        ->get();

        $establecimientos = Establecimiento::where('nombre', 'like', "%{$search}%")
        ->get();

        $categorias = Categoria::where('nombre', 'like', "%{$search}%")
        ->get();
        

        return Response::json([
            'productos' => $products,
            'establecimientos'=>$establecimientos,
            'categorias'=>$categorias
        ]);
    }

    public function saveSearch(Request $request)
    {
        // $search=$request->search;
        try {

            $search=$request->search;
            $user_id=$request->user_id;

            $s= new Search;
            $s->palabra=$search;
            $s->user_id=$user_id;
            $s->save();

        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }


    }

    public function getTop10(){
        $search=DB::table('search')
        ->select('search.palabra',DB::raw('COUNT(palabra) as count'))
        ->groupBy('palabra')
        ->orderBy('count', 'DESC')
        ->take(10)
        ->get();

        return Response::json([
            'busqueda' => $search
        ]);
    }

    public function getTop10ByUser(Request $request){

        try {
            $user_id=$request->user_id;

            $search=DB::table('search')
            ->select('search.palabra',DB::raw('COUNT(palabra) as count'))
            ->where('search.user_id', $user_id)
            ->groupBy('palabra')
            ->orderBy('count', 'DESC')
            ->take(10)->get();

            return Response::json([
                'busqueda' => $search
            ]);

        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }


        
    }
}
