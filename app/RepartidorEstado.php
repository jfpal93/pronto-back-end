<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RepartidorEstado extends Model
{
    //
    
    protected $table = 'repartidor_estado';
    protected $fillable = ['nombre'];

    public function users()
    {
        return $this->hasMany('App\RepartidorEstadoUser');
    }
}
