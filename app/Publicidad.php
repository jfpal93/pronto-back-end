<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publicidad extends Model
{
    protected $fillable = [
                            'nombre',
                            'linkImagen',
                            'orden'
                        ];

    public function detalle(){
       return $this->hasMany('App\PublicidadDetalle');
    }
}
