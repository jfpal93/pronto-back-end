<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidoCarritosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido_carritos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('subtotal');
            $table->float('total');
            $table->float('iva');
            $table->string('estado')->nullable();
            $table->integer('tiempo')->nullable();

            $table->bigInteger('establecimiento_id')->unsigned();
            $table->foreign('establecimiento_id')->references('id')->on('establecimientos')->onDelete('cascade');

            $table->bigInteger('carrito_id')->unsigned();
            $table->foreign('carrito_id')->references('id')->on('carritos')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedido_carritos');
    }
}
