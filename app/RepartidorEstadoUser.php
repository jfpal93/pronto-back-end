<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RepartidorEstadoUser extends Model
{
    //

    protected $table = 'repartidor_estado_user';
    protected $fillable = ['user_id', 'estado_repartidor_id'];

    public function repartidorEstado()
	{
		return $this->belongsTo('App\RepartidorEstado');
    }

    public function User()
	{
		return $this->belongsTo('App\User');
    }
}
