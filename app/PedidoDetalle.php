<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PedidoDetalle extends Model
{
    protected $table = 'pedido_detalles';
    protected $fillable = ['pedido_id', 'producto_id','cant'];

    public function pedido()
	{
		return $this->belongsTo('App\Pedido');
    }

    public function producto()
	{
		return $this->belongsTo('App\Producto');
	}
}
