<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;
use Exception;
use App\RepartidorEstadoUser;
use App\Orden;
use App\FirebaseToken;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {

            $users = User::whereHas('roles', function($q) {
                $q->where('name', '!=', 'client');
            })->with('roles')->get();
            return $users;
        } catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros problema: '.$e->getMessage()], 500);
        }
    }

    public function getClients()
    {
        try {
            

            $users = User::whereHas('roles', function($q) {
                $q->where('name','client');
            })->with('roles')->get();
            return $users;
        } catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros problema: '.$e->getMessage()], 500);
        }
    }

    public function getRepartidores()
    {
        try {

            $myArray = []; 

            $users = User::whereHas('roles', function($q) {
                $q->where('name','repartidor');
            })->with('roles')->get();

            foreach($users as $u)
            {
                $rep=RepartidorEstadoUser::where('user_id',$u->id)->first();
                $ordenesRep=Orden::where('repartidor_id',$u->id)
                ->where('ordens.estado','DRCA')
                ->orWhere('ordens.estado','TS')
                ->orWhere('ordens.estado','DA');
                if($rep->estado_repartidor_id==1){
                    $uTemp=array(
                        'user'=>$u,
                        'count'=>$ordenesRep->count(),
                        'estado'=>"Disponible"
                    );
                    array_push($myArray, $uTemp);
                }

            }
            return $myArray;
        } catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $user = User::with('roles')->findOrFail($id);
            return $user;
        } catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registro'], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $user = User::findOrFail($id);
            if($request->name != null)
                $user->name = $request->name;
            if($request->establecimiento_id != null)
                $user->establecimiento_id = $request->establecimiento_id;
            if($request->roles != null){
                $role = $user->roles->pluck('name')->toArray();
                $user->removeRole($role[0]);
                $user->assignRole($request->roles[0]['name']);
            }
            $user->direccion = $request->direccion;
            $user->telefono = $request->telefono;
            $user->lat = $request->lat;
            $user->lng = $request->lng;

            if($request->password != null)
                $user->password = $request->password;
            if($request->email != null)
                $user->email = $request->email;
            if($request->roles[0]['name']=='repartidor'){
                $user->linkImagen = $request->linkImagen;
            }
            $user->save();

            return $user;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function updateToken(Request $request, $usrId) {

        try {

            $user = User::findOrFail($usrId);
            $fbTOK=new FirebaseToken;
            $fbTOK->user_id	=$user->id;
            $fbTOK->firebase_token=$request->firebase_token;
            $fbTOK->save();

            // $user = User::findOrFail($usrId);
            // $user->firebase_token = $request->firebase_token;
            // $user->save();
            // return $user;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            User::destroy($id);
            return response()->json(['message' => 'usuario eliminado'], 200);
        } catch(Exception $e) {
            return response()->json(['error' => 'no se pudo eliminar registro'], 500);
        }
    }
}
