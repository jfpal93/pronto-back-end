<?php

use Illuminate\Database\Seeder;
use App\RepartidorEstado;


class RepartidorEstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $repEstado1 = RepartidorEstado::create(['nombre' => 'disponible']);
        $repEstado2 = RepartidorEstado::create(['nombre' => 'nodisponible']);
    }
}
