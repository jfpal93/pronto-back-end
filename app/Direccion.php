<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Direccion extends Model
{
    protected $table = 'direccions';
    protected $fillable = ['direccion','nombre','lat','lng', 'user_id','selected'];

    public function user()
	{
		return $this->belongsTo('App\User');
	}
}
