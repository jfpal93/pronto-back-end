<?php

namespace App\Http\Controllers;

use App\Publicidad;
use App\PublicidadDetalle;
use Illuminate\Http\Request;
use Exception;

class PublicidadController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index']]);
    }

    public function index()
    {
        try {
            $publicidads = Publicidad::with('detalle')->get();
            return $publicidads;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    public function store(Request $request)
    {
        try {
            $publicidad = Publicidad::create($request->all());
            foreach($request->detalle as $prodId) {
                $detalle = new PublicidadDetalle;
                $detalle->publicidad_id = $publicidad->id;
                $detalle->producto_id = $prodId;

                $publicidad->detalle()->save($detalle);
            }
            return $publicidad;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function show($id)
    {
        try {
            $publicidad = Publicidad::findOrFail($id);
            return $publicidad;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }


    public function update(Request $request, $id)
    {
        try {
            $publicidad = Publicidad::findOrFail($id);

            $publicidad->nombre = $request->nombre;
            $publicidad->orden = $request->orden;
            $publicidad->linkImagen = $request->linkImagen;

            if( $publicidad->detalle != null && $request->detalle != null ){
                foreach ($publicidad->detalle() as $detalle) {
                    $detalle->delete();
                }
            }

            if( $request->detalle != null ) {
                foreach($request->detalle as $prodId) {
                    $detalle = new PublicidadDetalle;
                    $detalle->publicidad_id = $publicidad->id;
                    $detalle->producto_id = $prodId;
                    $publicidad->detalle()->save($detalle);
                }
            }
            $publicidad->save();

            return $publicidad;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function destroy($id)
    {
        try {
            Publicidad::destroy($id);
            return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
