<?php

use Illuminate\Database\Seeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try
        {
            $this->call(RolesPermissionsSeeder::class);
            $this->call(RepartidorEstadoSeeder::class);
        }
        catch(\Exception $e)
        {   //Error de Base
            DB::rollback();
            printf( "Error al ejecutar seeders, restaurando los cambios creados. \n" );
            printf("Error: ". $e->getMessage()."\n");
            printf("Archivo: ". $e->getFile()."\n");
            printf("Linea: ". $e->getLine()."\n");
        }
        DB::commit();

    }
}
