<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SugerenciaDetalle extends Model
{
    protected $fillable = [];

    public function sugerencia()
	{
		return $this->belongsTo('App\Sugerencia');
	}
}
