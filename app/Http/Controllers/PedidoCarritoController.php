<?php

namespace App\Http\Controllers;

use App\PedidoCarrito;
use Illuminate\Http\Request;

class PedidoCarritoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PedidoCarrito  $pedidoCarrito
     * @return \Illuminate\Http\Response
     */
    public function show(PedidoCarrito $pedidoCarrito)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PedidoCarrito  $pedidoCarrito
     * @return \Illuminate\Http\Response
     */
    public function edit(PedidoCarrito $pedidoCarrito)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PedidoCarrito  $pedidoCarrito
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PedidoCarrito $pedidoCarrito)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PedidoCarrito  $pedidoCarrito
     * @return \Illuminate\Http\Response
     */
    public function destroy(PedidoCarrito $pedidoCarrito)
    {
        //
    }
}
