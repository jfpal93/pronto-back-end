<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create(['name' => 'Admin', 'email'=> 'admin@admin.com', 'password' => 'prontoMIL@GRO2019']);
        $admin = Role::create(['name' => 'admin']);
        $user->assignRole($admin);

        $client = Role::create(['name' => 'client']);
        $adminEst = Role::create(['name' => 'adminEstablecimiento']);
        $secretarioEstablecimiento = Role::create(['name' => 'secretarioEstablecimiento']);
        $secretarioGlobal = Role::create(['name' => 'secretarioGlobal']);
        $repartidor = Role::create(['name' => 'repartidor']);
    }
}
