<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorito extends Model
{
    protected $fillable = ['nombre'];

    public function detalle(){
       return $this->hasMany('App\FavoritoDetalle');
    }
}
