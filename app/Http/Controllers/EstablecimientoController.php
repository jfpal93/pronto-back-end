<?php

namespace App\Http\Controllers;

use App\Establecimiento;
use Illuminate\Http\Request;
use Exception;

class EstablecimientoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index']]);
    }
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $establecimientos = Establecimiento::all();
            return $establecimientos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $establecimiento = Establecimiento::create($request->all());
            return $establecimiento;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\establecimiento  $establecimiento
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $establecimiento = Establecimiento::findOrFail($id);
            return $establecimiento;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\establecimiento  $establecimiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $establecimiento = Establecimiento::findOrFail($id);
            $establecimiento->nombre = $request->nombre;
            $establecimiento->categoria_id = $request->categoria_id;
            $establecimiento->direccion = $request->direccion;
            $establecimiento->calificacion = $request->calificacion;
            $establecimiento->lat = $request->lat;
            $establecimiento->lng = $request->lng;

            $establecimiento->linkImagen = $request->linkImagen;

            $establecimiento->save();
            return $establecimiento;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\establecimiento  $establecimiento
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Establecimiento::destroy($id);
            return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
