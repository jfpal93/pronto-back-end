<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PedidoCarrito extends Model
{
    protected $table = 'pedido_carritos';
    protected $fillable = ['subtotal', 'estado', 'tiempo', 'establecimiento_id',
                            'carrito_id'];

    public function establecimiento()
    {
        return $this->belongsTo('App\Establecimiento');
    }

    public function carrito()
	{
		return $this->belongsTo('App\Carrito');
    }

    public function detalles()
    {
        return $this->hasMany('App\PedidoCarritoDetalle');
    }
}
