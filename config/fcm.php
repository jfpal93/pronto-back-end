<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => false,

    'http' => [
        'server_key' => env('FCM_SERVER_KEY', 'AAAA1-NUKvk:APA91bEBcVqVqIdRItCBnHoGz5liZqt6VAzZmTLA-b4s0NS2PHMTXdzEJL7rvuwk_e8nP83CO0iniHkLIzY8O8QslOzodPjzMpV1DZ2PzoQj9Rycye2zOa4_T5fr6gSVL5NKp_cpwZMr'),
        'sender_id' => env('FCM_SENDER_ID', '927231912697'),
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];
