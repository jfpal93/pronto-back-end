<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrito extends Model
{
    protected $table = 'carritos';
    protected $fillable = ['costo_delivery', 'estado', 'tiempo', 'subtotal', 'total',
                            'establecimiento_id', 'user_id','cant'];

    public function establecimiento()
	{
		return $this->belongsTo('App\Establecimiento');
    }

    public function user()
	{
		return $this->belongsTo('App\User');
    }

    public function pedido_carrito_detalle()
	{
		return $this->hasMany('App\PedidoCarritoDetalle');
    }
}
