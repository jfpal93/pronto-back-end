<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Orden;

use App\Pedido;
use App\User;
use App\PedidoDetalle;
use DB;
use Response;
use Illuminate\Http\Request;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use DateTime;
use Carbon\Carbon;
use App\FirebaseToken;


class CancelOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $orden;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Orden $orden)
    {
        //
        $this->orden = $orden;
        // $this->orden->job_id=$this->job->getJobId();
        // $this->orden->update();
        // echo $this->job->getJobId();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //

        try {
            $ord=Orden::where('id',$this->orden->id)->first();
            if($ord->estado==="DR"){
                $this->orden->estado="cancelado";
                $codigo_orden=$this->orden->codigo;
                $this->orden->update();
    
    
                foreach(Pedido::where('orden_id',$this->orden->id)->get() as $pc)
                {
                    $pc->estado="cancelado";
                    $pc->update();
                    //Notificacion Establecimiento
    
                    // $user=User::where('establecimiento_id',$pc->establecimiento_id)->first();
                    $users=User::where('establecimiento_id',$pc->establecimiento_id)->pluck('id')->toArray();
    
                    $optionBuilder = new OptionsBuilder();
                    $optionBuilder->setTimeToLive(60*20);
    
                    $notificationBuilder = new PayloadNotificationBuilder('¡Pedido Cancelado!');
                    $msg="El pedido $codigo_orden fue cancelado...";
                    $notificationBuilder->setBody($msg)
                                        ->setSound('default');
    
                    $dataBuilder = new PayloadDataBuilder();
                    $dataBuilder->addData(['canceled' => 'my_data']);
    
                    $option = $optionBuilder->build();
                    $notification = $notificationBuilder->build();
                    $data = $dataBuilder->build();
    
                    $userTokens=FirebaseToken::whereIn('user_id',$users)->pluck('firebase_token')->toArray();
    
                    // echo $user->firebase_token;
                    if(count($userTokens)>0){
    
                        // $token = $user->firebase_token;
    
                        $downstreamResponse = FCM::sendTo($userTokens, $option, $notification, $data);
                        $downstreamResponse->numberSuccess();
                        $downstreamResponse->numberFailure();
                        $downstreamResponse->numberModification();
                    }
    
                    
                }   
                //Notificacion Administrador y secretario
    
                // $user=User::where('id', 1)->first();
    
                $users = DB::table('users')
                ->join('model_has_roles', 'users.id', 'model_has_roles.model_id')
                ->select( 
                    'users.id as id'
                    )
                ->where('model_has_roles.role_id', 1)
                ->orWhere('model_has_roles.role_id', 5)
                ->pluck('id')->toArray();
    
                $optionBuilder = new OptionsBuilder();
                $optionBuilder->setTimeToLive(60*20);
    
                $notificationBuilder = new PayloadNotificationBuilder('¡Pedido Cancelado!');
                $notificationBuilder->setBody("El pedido $codigo_orden fue cancelado...")
                                    ->setSound('default');
    
                $dataBuilder = new PayloadDataBuilder();
                $dataBuilder->addData(['a_data' => 'my_data']);
    
                $option = $optionBuilder->build();
                $notification = $notificationBuilder->build();
                $data = $dataBuilder->build();
    
                $userTokens=FirebaseToken::whereIn('user_id',$users)->pluck('firebase_token')->toArray();
    
    
                // echo $user->firebase_token;
                if(count($userTokens)>0){
    
                    // $token = $user->firebase_token;
    
                    $downstreamResponse = FCM::sendTo($userTokens, $option, $notification, $data);
                    $downstreamResponse->numberSuccess();
                    $downstreamResponse->numberFailure();
                    $downstreamResponse->numberModification();
                }  
    
                //Notificacion Usuario
                
                // $userapp=User::where('id',$this->orden->user_id)->first();
                $optionBuilder = new OptionsBuilder();
                $optionBuilder->setTimeToLive(60*20);
    
                $notificationBuilder = new PayloadNotificationBuilder('¡Tu pedido fue cancelado por inactividad!');
                $notificationBuilder->setBody('Esto sucede a los 30 min de no haber respondido tu requerimiento. Gracias por su comprensión')
                                    ->setSound('default');
    
                $dataBuilder = new PayloadDataBuilder();
                $dataBuilder->addData(['a_data' => 'my_data']);
    
                $option = $optionBuilder->build();
                $notification = $notificationBuilder->build();
                $data = $dataBuilder->build();
    
                $userTokens=FirebaseToken::where('user_id',$this->orden->user_id)->pluck('firebase_token')->toArray();
    
                // echo $user->firebase_token;
                if(count($userTokens)>0){
    
                    // $token = $userapp->firebase_token;
    
                    $downstreamResponse = FCM::sendTo($userTokens, $option, $notification, $data);
                    $downstreamResponse->numberSuccess();
                    $downstreamResponse->numberFailure();
                    $downstreamResponse->numberModification();
                }
            }
            
            
        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
