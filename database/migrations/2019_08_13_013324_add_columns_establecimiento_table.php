<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsEstablecimientoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('establecimientos', function (Blueprint $table) {
            $table->text('direccion')->nullable();
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->integer('calificacion')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('establecimientos', function (Blueprint $table) {
            $table->dropColumn('direccion')->nullable();
            $table->dropColumn('lat')->nullable();
            $table->dropColumn('lng')->nullable();
            $table->dropColumn('calificacion')->nullable();
        });
    }
}
