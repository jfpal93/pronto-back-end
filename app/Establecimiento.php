<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Establecimiento extends Model
{
    protected $fillable = ['nombre','categoria_id', 'user_id', 'linkImagen', 'direccion', 'calificacion', 'lat', 'lng'];

    public function user()
	{
		return $this->hasMany('App\User');
	}
}
