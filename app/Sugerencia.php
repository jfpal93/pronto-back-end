<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sugerencia extends Model
{
    protected $fillable = ['categoria_id'];

    public function detalle(){
        return $this->hasMany('App\SugerenciaDetalle');
     }
}
