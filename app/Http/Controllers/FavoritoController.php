<?php

namespace App\Http\Controllers;

use App\Favorito;
use App\FavoritoDetalle;
use Illuminate\Http\Request;
use Exception;

class FavoritoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $favoritos = Favorito::with('detalle')->get();
            return $favoritos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $favorito = Favorito::create($request->all());
            foreach($request->detalle as $prodId) {
                $detalle = new FavoritoDetalle;
                $detalle->favorito_id = $favorito->id;
                $detalle->producto_id = $prodId;

                $favorito->detalle()->save($detalle);
            }
            return $favorito;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\favorito  $favorito
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $favorito = Favorito::findOrFail($id);
            return $favorito;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\favorito  $favorito
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $favorito = Favorito::with('detalle')->find($id);
            // $favorito->nombre = $request->nombre;
            //dd($favorito->detalle);
            $detalleArray=$request->detalle;
            if($favorito->detalle != null && $request->detalle != null) {
                foreach ($favorito->detalle as $detalle) {
                    $detalle->delete();
                    $favorito->save();
                }
            }
            if( $request->detalle != null ) {
                foreach($request->detalle as $prodId) {
                    $detalle = new FavoritoDetalle;
                    $detalle->favorito_id = $favorito->id;
                    $detalle->producto_id = $prodId;

                    $favorito->detalle()->save($detalle);
                    $favorito->save();
                }
            }

            if(is_array($detalleArray) ) {
                if(count($detalleArray)==0){
                    // return response()->json(['array' => 'array'], 500);
                    foreach ($favorito->detalle as $detalle) {
                        $detalle->delete();
                        $favorito->save();
                        // $favorito->detalle()->save($detalle);
                    }
                }
                
                
            }
            

            return $favorito;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\favorito  $favorito
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Favorito::destroy($id);
            return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
