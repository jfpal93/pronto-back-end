<?php

namespace App\Http\Controllers;

use App\Sugerencia;
use App\SugerenciaDetalle;
use Illuminate\Http\Request;
use Exception;

class SugerenciaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $sugerencias = Sugerencia::with('detalle')->get();
            return $sugerencias;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $sugerencia = Sugerencia::create($request->all());
            foreach($request->detalle as $prodId) {
                $detalle = new SugerenciaDetalle;
                $detalle->sugerencia_id = $sugerencia->id;
                $detalle->producto_id = $prodId;

                $sugerencia->detalle()->save($detalle);
            }
            return $sugerencia;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\sugerencia  $sugerencia
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $sugerencia = Sugerencia::findOrFail($id);
            return $sugerencia;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\sugerencia  $sugerencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $sugerencia = Sugerencia::find($id);
            $sugerencia->categoria_id = $request->categoria_id;

            if($sugerencia->detalle != null && $request->detalle != null) {
                foreach ($sugerencia->detalle as $detalle) {
                    $detalle->delete();
                }
            }

            foreach($request->detalle as $prodId) {
                $detalle = new SugerenciaDetalle;
                $detalle->sugerencia_id = $sugerencia->id;
                $detalle->producto_id = $prodId;

                $sugerencia->detalle()->save($detalle);
            }

            $sugerencia->save();
            return $sugerencia;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\sugerencia  $sugerencia
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Sugerencia::destroy($id);
            return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
