<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $fillable = [
                            'nombre',
                            // 'categoria_id',
                            'establecimiento_id',
                            'descripcion',
                            'linkImagen',
                            'precio',
                            'disponible'
                        ];

    public function categoria()
	{
		return $this->belongsTo('App\Categoria');
	}
}
