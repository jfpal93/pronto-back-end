<?php

namespace App\Http\Controllers;

// use Illuminate\Support\Facades\Log;

use App\Carrito;
use App\User;
use App\Producto;
use Illuminate\Http\Request;
use App\PedidoCarrito;
use App\PedidoCarritoDetalle;
use DB;
use Response;

use App\Orden;
use App\Pedido;
use App\PedidoDetalle;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use App\FirebaseToken;

class CarritoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    
    public function getCarritoUsuario($userId)
    {
        try {
            $products = DB::table('carritos')
                ->join('pedido_carritos', 'carritos.id', 'pedido_carritos.carrito_id')
                ->join('pedido_carrito_detalles', 'pedido_carritos.id', 'pedido_carrito_detalles.pedido_carrito_id')
                ->join('productos', 'productos.id', 'pedido_carrito_detalles.producto_id')
                ->join('establecimientos', 'establecimientos.id', 'productos.establecimiento_id')
                ->join('categorias', 'categorias.id', 'establecimientos.categoria_id')
                ->select( 
                    'productos.id',
                    'productos.nombre',
                    'productos.linkImagen',
                    'productos.descripcion',
                    'productos.precio',
                    'productos.disponible',
                    'categorias.id AS categoria_id',
                    'categorias.nombre AS categoria',
                    'productos.establecimiento_id', 
                    'establecimientos.nombre AS establecimiento',
                    'pedido_carrito_detalles.id AS pedido_carrito_id',
                    'pedido_carrito_detalles.cant AS cant' 
                    )
                ->where('carritos.user_id', $userId)
                ->get();


                $carrito = Carrito::where('user_id', $userId)->first();
            return Response::json(array(
                'products'=>$products,
                'total'=>$carrito->total,
                'user_id'=>$carrito->user_id,
                'cant'=>$carrito->cant,
                'carrito_id'=>$carrito->id));
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function postCarritoUsuario(Request $request, $userId)
    {
        try {

            $data = new \Illuminate\Support\Collection($request->data);

            $carrito = Carrito::where('user_id', $userId)->first();

            if($carrito == null)
            {
                $carrito = new Carrito;
                $carrito->subtotal = $request->subtotal;
                $carrito->total = $request->total;
                $carrito->user_id = $userId;

                $carrito->save();
                foreach($data->groupBy('establecimiento_id') as $key => $prod) {
                    $pc = new PedidoCarrito;

                    $pc->establecimiento_id = $key;
                    $pc->carrito_id = $carrito->id;
                    $pc->subtotal = $prod->subtotal;
                    $pc->iva = $prod->iva;
                    $pc->total = $prod->total;
                    $pc->save();
                    foreach ($prod->productos as $key => $p) {
                        $pedido_carrito = new PedidoCarritoDetalle;

                        $pedido_carrito->pedido_id = $pc->id;
                        $pedido_carrito->producto_id = $p->producto_id;
                        $pedido_carrito->save();
                    }
                    
                }
            } else {
                $carrito->destroy();
                $carrito = new Carrito;
                $carrito->subtotal = $request->subtotal;
                $carrito->total = $request->total;
                $carrito->user_id = $userId;

                $carrito->save();
                foreach($data->groupBy('establecimiento_id') as $key => $prod) {
                    $pc = new PedidoCarrito;

                    $pc->establecimiento_id = $key;
                    $pc->carrito_id = $carrito->id;
                    $pc->subtotal = $prod->subtotal;
                    $pc->iva = $prod->iva;
                    $pc->total = $prod->total;
                    $pc->save();
                    foreach ($prod->productos as $key => $p) {
                        $pedido_carrito = new PedidoCarritoDetalle;

                        $pedido_carrito->pedido_id = $pc->id;
                        $pedido_carrito->producto_id = $p->producto_id;
                        $pedido_carrito->save();
                    }
                    
                }
            }

            return $carrito;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function addProductCarritoUsuario(Request $request, $userId)
    {

        //Request data
        // IdProduct 
        // cant
        try {

            $cant=$request->cant;
            $IdProduct=$request->IdProduct;

            $data = new \Illuminate\Support\Collection($request->data);
            $carrito = Carrito::where('user_id', $userId)->first();
            $product = Producto::find($IdProduct);

            $pc = PedidoCarrito::where('establecimiento_id', $product->establecimiento_id)
            ->where('carrito_id', $carrito->id)->get();


            if($pc->count())
            {
                $carrito->total = ($product->precio * $cant) + $carrito->total;
                $carrito->cant=$carrito->cant + $cant;
                $carrito->update();
                

                foreach($pc as $pedcart) {
                    if($pedcart->establecimiento_id==$product->establecimiento_id){
                        $pedcart->subtotal=($product->precio * $request->cant) + $pedcart->subtotal;
                        $pedcart->update();

                        $pedcartdet = PedidoCarritoDetalle::where('pedido_carrito_id', $pedcart->id)
                        ->where('producto_id', $IdProduct)
                        ->first();

                        if (empty($pedcartdet)) {
                            $pedido_carrito = new PedidoCarritoDetalle;

                            $pedido_carrito->pedido_carrito_id = $pedcart->id;
                            $pedido_carrito->producto_id = $product->id;
                            $pedido_carrito->cant = $request->cant;
                            $pedido_carrito->save();
                        }
                        else{
                            $pedcartdet->cant=$pedcartdet->cant + $request->cant;
                            $pedcartdet->update();
                        }

                        


                    }

                }
                return;
            }
            else{
                $carrito->total = ($product->precio * $cant)+ $carrito->total;
                $carrito->cant=$carrito->cant +$cant;
                $carrito->update();

                $pc = new PedidoCarrito;
                $pc->establecimiento_id = $product->establecimiento_id;
                $pc->carrito_id = $carrito->id;
                $pc->subtotal = ($product->precio * $cant);
                $pc->save();

                $pedido_carrito = new PedidoCarritoDetalle;

                $pedido_carrito->pedido_carrito_id = $pc->id;
                $pedido_carrito->producto_id = $product->id;
                $pedido_carrito->cant = $cant;
                $pedido_carrito->save();
                return;


                
            }

        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }

    }

    public function deleteProductCarritoUsuario(Request $request, $userId)
    {

        try {

            $cant=$request->cant;
            $IdProduct=$request->IdProduct;

            $data = new \Illuminate\Support\Collection($request->data);
            $carrito = Carrito::where('user_id', $userId)->first();
            $product = Producto::find($IdProduct);

            $pc = PedidoCarrito::where('establecimiento_id', $product->establecimiento_id)
            ->where('carrito_id', $carrito->id)->get();                
                
            $flag = false;
            $pedcartid;
            $pedcartdetid;

            foreach($pc as $pedcart) {
                if($pedcart->establecimiento_id==$product->establecimiento_id){
                    $carrito->total = $carrito->total - ($product->precio * $cant);

                    if($carrito->cant - $cant >=0){
                        $carrito->cant=$carrito->cant - $cant;
                        $carrito->update();
                    }

                    $pedcartdet = PedidoCarritoDetalle::where('pedido_carrito_id', $pedcart->id)->first();

                    if($pedcartdet->cant - $request->cant > 0){
                        $pedcart->subtotal= $pedcart->subtotal - ($product->precio * $request->cant) ;
                        $pedcart->update();

                        

                        $pedcartdet->cant=$pedcartdet->cant - $request->cant;
                        $pedcartdet->update();

                    }
                    else{
                        $flag=true;
                        $pedcartid=$pedcart->id;
                        $pedcartdetid=$pedcartdet->id;
                    }
                    


                }

            }
            if($flag){
                PedidoCarrito::destroy($pedcartid);
                PedidoCarritoDetalle::destroy($pedcartdetid);
            }
            return;

        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }

    }

    public function placeOrder(Request $request, $userId){
        try {

            $codigoOrden;
            $total;

            $carrito = Carrito::where('user_id', $userId)->first();   
            $total=$carrito->total; 

            // $orden = array(
            // 'subtotal' => $carrito->total, 
            // 'total' => $carrito->total, 
            // 'user_id'=>$carrito->user_id,
            // 'cant'=>$carrito->cant,
            // 'estado'=>"procesando",
            // 'tiempo'=>0,
            // // 'repartidor_id'=>null,
            // 'direccion_id'=>$request->dirId);

            // $ordenCreated=Orden::create($orden);

            $ordenCreated = new Orden;

            $ordenCreated->subtotal =$carrito->total;
            $ordenCreated->total = $carrito->total;
            $ordenCreated->user_id = $carrito->user_id;
            $ordenCreated->cant = $carrito->cant;
            $ordenCreated->estado = "procesando";
            $ordenCreated->tiempo = 0;
            $ordenCreated->direccion_id = $request->dirId;
            $ordenCreated->save();
            
            $ordenId=$ordenCreated->id;

            foreach(PedidoCarrito::where('carrito_id',$carrito->id)->get() as $pc)
            {

                $p = array(
                'subtotal' => $pc->subtotal, 
                'total' => $pc->subtotal, 
                'iva'=>0.0,
                'estado'=>"procesando",
                'tiempo'=>0,
                'establecimiento_id'=>$pc->establecimiento_id,
                'orden_id'=>$ordenId);
                
                $pedId=Pedido::create($p)->id;

                foreach(PedidoCarritoDetalle::where('pedido_carrito_id',$pc->id)->get() as $pcd)
                {

                    $pd = array(
                    'pedido_id' =>$pedId, 
                    'producto_id' => $pcd->producto_id, 
                    'cant'=>$pcd->cant);
                    
                    PedidoDetalle::create($pd);

                }

               

            }

            
            $pcdDEL=PedidoCarrito::where('carrito_id',$carrito->id)->get();
            foreach($pcdDEL as $pc)
            {
                do {
                    $deleted = PedidoCarritoDetalle::where('pedido_carrito_id',$pc->id)->limit(5)->delete();
                    sleep(2);
                } while ($deleted > 0);
            }
            

            PedidoCarrito::where('carrito_id',$carrito->id)->delete();
            Carrito::where('user_id', $userId)->delete();

            

            $carrito = array('subtotal' => 0.0, 'total' => 0.0 , 'user_id'=>$userId,'cant'=>0);
            Carrito::create($carrito);

            $month=$ordenCreated->created_at->format('M');
            $day=$ordenCreated->created_at->format('d');
            $year=$ordenCreated->created_at->format('Y');

            $ordenNum=$ordenId*674;

            $ordenCreated->codigo="pronto-{$ordenNum}-{$day}-{$month}-{$year}";
            $codigoOrden="pronto-{$ordenNum}-{$day}-{$month}-{$year}";

            $ordenCreated->update();

            foreach(Pedido::where('orden_id',$ordenId)->get() as $pc)
            {
                // $user=User::where('establecimiento_id',$pc->establecimiento_id)->first();
                $users=User::where('establecimiento_id',$pc->establecimiento_id)->pluck('id')->toArray();

                $optionBuilder = new OptionsBuilder();
                $optionBuilder->setTimeToLive(60*20);
    
                $notificationBuilder = new PayloadNotificationBuilder('¡Nueva Orden!');
                $notificationBuilder->setBody('')
                                    ->setSound('default');
    
                $dataBuilder = new PayloadDataBuilder();
                $dataBuilder->addData(['a_data' => 'my_data']);
    
                $option = $optionBuilder->build();
                $notification = $notificationBuilder->build();
                $data = $dataBuilder->build();
    
                // echo $user->firebase_token;

                // $userTokens=FirebaseToken::where('user_id',$user->id)->get();
                $userTokens=FirebaseToken::whereIn('user_id',$users)->pluck('firebase_token')->toArray();
                if(count($userTokens)>0){
                    // $token = $user->firebase_token;
        
                    $downstreamResponse = FCM::sendTo($userTokens, $option, $notification, $data);
                    $downstreamResponse->numberSuccess();
                    $downstreamResponse->numberFailure();
                    $downstreamResponse->numberModification();
                }
            }

            // return $codigoOrden;

            return Response::json(array(
                'codigo'=>$codigoOrden,
                'total'=>$total));


        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }

    }
}
