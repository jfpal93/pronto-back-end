<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavoritoDetalle extends Model
{
    protected $fillable = [];

    public function favorito()
	{
		return $this->belongsTo('App\Favorito');
	}
}
