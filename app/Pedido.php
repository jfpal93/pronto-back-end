<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $table = 'pedidos';
    protected $fillable = ['subtotal', 'total', 'iva', 'estado', 'tiempo', 'establecimiento_id',
                            'orden_id'];

    public function establecimiento()
    {
        return $this->belongsTo('App\Establecimiento');
    }

    public function orden()
	{
		return $this->belongsTo('App\Orden');
    }

    public function detalles()
    {
        return $this->hasMany('App\PedidoDetalle');
    }
}
