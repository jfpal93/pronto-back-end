<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarritosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carritos', function (Blueprint $table) {
            $table->bigIncrements('id');
            //$table->float('costo_delivery')->nullable();
            $table->string('estado')->nullable();
            //$table->integer('tiempo');
            $table->float('subtotal');
            $table->float('total');
            // $table->bigInteger('establecimiento_id')->unsigned();
            // $table->foreign('establecimiento_id')->references('id')->on('establecimientos')->onDelete('cascade');

            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->bigInteger('direccion_id')->unsigned()->nullable();
            $table->foreign('direccion_id')->references('id')->on('direccions')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carritos');
    }
}
