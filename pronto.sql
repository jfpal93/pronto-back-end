-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 29, 2020 at 11:45 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pronto`
--

-- --------------------------------------------------------

--
-- Table structure for table `carritos`
--

CREATE TABLE `carritos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `estado` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtotal` double(8,2) NOT NULL,
  `total` double(8,2) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `direccion_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cant` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carritos`
--

INSERT INTO `carritos` (`id`, `estado`, `subtotal`, `total`, `user_id`, `direccion_id`, `created_at`, `updated_at`, `cant`) VALUES
(1, NULL, 0.00, 10.00, 3, NULL, '2019-12-08 07:08:00', '2019-12-08 21:59:27', 1),
(3, NULL, 0.00, 0.00, 4, NULL, '2019-12-08 21:58:04', '2019-12-08 21:58:04', 0),
(5, NULL, 0.00, 10.00, 13, NULL, '2019-12-08 22:33:57', '2019-12-08 22:51:03', 1),
(7, NULL, 0.00, 0.00, 14, NULL, '2019-12-21 16:17:40', '2019-12-21 16:17:40', 0),
(8, NULL, 0.00, 0.00, 17, NULL, '2019-12-22 13:45:29', '2019-12-22 13:45:29', 0),
(9, NULL, 0.00, 0.00, 18, NULL, '2019-12-22 13:48:51', '2019-12-22 13:48:51', 0),
(10, NULL, 0.00, 0.00, 19, NULL, '2019-12-22 13:51:01', '2019-12-22 13:51:01', 0),
(11, NULL, 0.00, 25.00, 20, NULL, '2019-12-22 13:53:55', '2019-12-22 13:53:57', 3),
(12, NULL, 0.00, 50.00, 21, NULL, '2019-12-22 13:56:31', '2019-12-22 13:56:32', 6),
(13, NULL, 0.00, 10.00, 22, NULL, '2019-12-22 14:01:16', '2019-12-22 14:01:17', 1),
(14, NULL, 0.00, 20.00, 23, NULL, '2019-12-22 14:04:31', '2019-12-22 14:04:33', 2),
(15, NULL, 0.00, 0.00, 24, NULL, '2019-12-22 14:15:06', '2019-12-22 14:15:06', 0),
(16, NULL, 0.00, 40.00, 25, NULL, '2019-12-22 22:34:03', '2019-12-22 22:34:12', 6),
(17, NULL, 0.00, 30.00, 26, NULL, '2019-12-22 22:36:46', '2019-12-22 22:36:48', 4),
(18, NULL, 0.00, 40.00, 27, NULL, '2019-12-22 22:39:41', '2019-12-22 22:39:42', 4),
(19, NULL, 0.00, 30.00, 28, NULL, '2019-12-22 22:41:33', '2019-12-22 22:41:35', 4),
(20, NULL, 0.00, 10.00, 30, NULL, '2019-12-22 22:45:51', '2019-12-22 22:45:52', 1),
(21, NULL, 0.00, 40.00, 31, NULL, '2019-12-22 22:47:16', '2019-12-22 22:47:17', 4),
(22, NULL, 0.00, 15.00, 32, NULL, '2019-12-22 22:50:06', '2019-12-22 22:50:08', 2),
(23, NULL, 0.00, 20.00, 33, NULL, '2019-12-23 01:02:19', '2019-12-23 01:02:20', 2),
(24, NULL, 0.00, 440.00, 34, NULL, '2019-12-23 01:09:11', '2019-12-23 04:27:22', 52),
(25, NULL, 0.00, 0.00, 35, NULL, '2019-12-23 02:36:55', '2019-12-23 02:36:55', 0),
(26, NULL, 0.00, 0.00, 36, NULL, '2019-12-23 02:38:46', '2019-12-23 02:38:46', 0),
(27, NULL, 0.00, 0.00, 37, NULL, '2019-12-23 02:52:15', '2019-12-23 02:52:15', 0),
(28, NULL, 0.00, 240.00, 38, NULL, '2019-12-23 03:16:41', '2019-12-23 03:21:10', 32),
(29, NULL, 0.00, 20.00, 39, NULL, '2019-12-23 03:21:47', '2019-12-23 03:22:21', 4),
(30, NULL, 0.00, 10.00, 40, NULL, '2019-12-23 04:33:11', '2019-12-23 04:33:13', 1),
(31, NULL, 0.00, 40.00, 41, NULL, '2019-12-23 06:13:50', '2019-12-23 06:14:57', 4),
(32, NULL, 0.00, 40.00, 42, NULL, '2019-12-23 06:17:55', '2019-12-23 06:19:15', 4),
(33, NULL, 0.00, 15.00, 43, NULL, '2019-12-23 06:33:55', '2019-12-23 06:33:57', 2),
(34, NULL, 0.00, 30.00, 44, NULL, '2019-12-23 06:37:10', '2019-12-23 06:58:23', 5),
(35, NULL, 0.00, 0.00, 45, NULL, '2019-12-23 20:12:34', '2019-12-23 20:12:34', 0),
(36, NULL, 0.00, 0.00, 46, NULL, '2019-12-23 20:36:53', '2019-12-23 20:36:53', 0),
(37, NULL, 0.00, 25.00, 47, NULL, '2019-12-24 18:46:55', '2019-12-24 18:50:22', 4),
(38, NULL, 0.00, 0.00, 48, NULL, '2019-12-28 03:51:31', '2019-12-28 03:51:31', 0),
(39, NULL, 0.00, 0.00, 49, NULL, '2019-12-28 04:03:10', '2019-12-28 04:03:10', 0),
(40, NULL, 0.00, 0.00, 50, NULL, '2019-12-28 04:08:33', '2019-12-28 04:08:33', 0),
(42, NULL, 0.00, 0.00, 52, NULL, '2019-12-29 03:21:27', '2019-12-29 03:21:27', 0),
(44, NULL, 0.00, 5.00, 53, NULL, '2019-12-29 06:58:08', '2019-12-29 10:03:42', 1),
(47, NULL, 0.00, 0.00, 54, NULL, '2019-12-29 16:23:27', '2019-12-29 16:23:27', 0),
(49, NULL, 0.00, 0.00, 55, NULL, '2019-12-29 16:28:47', '2019-12-29 16:28:47', 0),
(52, NULL, 0.00, 20.00, 51, NULL, '2019-12-31 05:21:24', '2019-12-31 23:49:51', 3);

-- --------------------------------------------------------

--
-- Table structure for table `categorias`
--

CREATE TABLE `categorias` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `linkImagen` text COLLATE utf8mb4_unicode_ci,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categorias`
--

INSERT INTO `categorias` (`id`, `linkImagen`, `nombre`, `created_at`, `updated_at`) VALUES
(3, 'https://firebasestorage.googleapis.com/v0/b/prontoapp-5cec2.appspot.com/o/categorias%2F1575783725247_Bitmap.png?alt=media&token=66e61818-b6c8-4e64-9916-15f8acf73b6d', 'Categoría 1', '2019-12-08 10:44:21', '2019-12-08 10:44:21');

-- --------------------------------------------------------

--
-- Table structure for table `direccions`
--

CREATE TABLE `direccions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `direccion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nombre` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lng` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `selected` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `direccions`
--

INSERT INTO `direccions` (`id`, `direccion`, `user_id`, `created_at`, `updated_at`, `nombre`, `lat`, `lng`, `selected`) VALUES
(1, 'Via Perimetral', 4, '2019-12-08 21:46:10', '2019-12-08 21:46:53', 'Casa', '-2.0963138', '-79.9486993', 1),
(3, 'Via daule', 13, '2019-12-08 22:30:14', '2019-12-08 22:31:15', 'Trabajo', '-2.0963786', '-79.9486321', 1),
(4, 'skldjfaskfasdlkfj', 14, '2019-12-21 16:17:15', '2019-12-21 16:17:31', 'Casa', '0', '0', 1),
(5, 'via periemtral', 53, '2019-12-29 06:57:53', '2019-12-29 06:58:00', 'Casa', '-2.0959177', '-79.9490852', 1),
(6, 'villa España, Málaga MZ 2172 v 6', 51, '2019-12-29 07:04:46', '2019-12-31 04:48:00', 'casa', '-2.0801165', '-79.9216248', 1),
(7, 'hfhvhh', 54, '2019-12-29 16:22:43', '2019-12-29 16:23:12', 'casa', '-2.0938310068044843', '-79.9474781663908', 1),
(8, 'jdjwjdj', 55, '2019-12-29 16:28:30', '2019-12-29 16:28:36', 'casa', '-2.0901354846690507', '-79.94486537254755', 1);

-- --------------------------------------------------------

--
-- Table structure for table `establecimientos`
--

CREATE TABLE `establecimientos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `linkImagen` text COLLATE utf8mb4_unicode_ci,
  `direccion` text COLLATE utf8mb4_unicode_ci,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lng` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `calificacion` int(11) DEFAULT NULL,
  `categoria_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `establecimientos`
--

INSERT INTO `establecimientos` (`id`, `nombre`, `created_at`, `updated_at`, `linkImagen`, `direccion`, `lat`, `lng`, `calificacion`, `categoria_id`) VALUES
(3, 'Flore Store', '2019-12-08 13:19:38', '2019-12-08 13:20:15', 'https://firebasestorage.googleapis.com/v0/b/prontoapp-5cec2.appspot.com/o/establecimientos%2F1575793205851_Bitmap.png?alt=media&token=8a6909d6-d07f-4326-b361-79dba684e55a', 'ghjghjhjfhj', '-2.1367189482152114', '-79.59587696656496', NULL, 3),
(4, 'Jorge  Store', '2019-12-21 23:52:13', '2019-12-21 23:52:13', 'https://firebasestorage.googleapis.com/v0/b/prontoapp-5cec2.appspot.com/o/establecimientos%2F1576954325736_Bitmap.png?alt=media&token=18ff49dd-92d8-4598-9da3-bb2371741c22', 'askldfhasldkfhaslkdfhasdlkjf', '-2.141179034296226', '-79.59806564912111', NULL, 3),
(5, 'Gabriel Store', '2019-12-22 02:12:16', '2019-12-22 02:12:16', 'https://firebasestorage.googleapis.com/v0/b/prontoapp-5cec2.appspot.com/o/establecimientos%2F1576962730496_Bitmap.png?alt=media&token=43ee726d-2e96-4876-bb29-fb4cae4c3edc', 'dafhdfjfghfghfg', '-2.1413505758095166', '-79.60132721528322', NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `favoritos`
--

CREATE TABLE `favoritos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `favoritos`
--

INSERT INTO `favoritos` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(2, 'Favorito 1', '2019-12-29 22:10:31', '2019-12-29 22:10:31'),
(3, 'Favorito 2', '2019-12-29 22:10:56', '2019-12-29 22:10:56'),
(4, 'Favorito 3', '2019-12-29 22:11:21', '2019-12-29 22:11:21'),
(5, 'Favorito 4', '2019-12-29 22:11:42', '2019-12-29 22:11:42');

-- --------------------------------------------------------

--
-- Table structure for table `favorito_detalles`
--

CREATE TABLE `favorito_detalles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `favorito_id` bigint(20) UNSIGNED NOT NULL,
  `producto_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `favorito_detalles`
--

INSERT INTO `favorito_detalles` (`id`, `favorito_id`, `producto_id`, `created_at`, `updated_at`) VALUES
(27, 2, 1, '2019-12-29 22:10:31', '2019-12-29 22:10:31'),
(28, 2, 2, '2019-12-29 22:10:31', '2019-12-29 22:10:31'),
(29, 2, 8, '2019-12-29 22:10:31', '2019-12-29 22:10:31'),
(30, 3, 8, '2019-12-29 22:10:56', '2019-12-29 22:10:56'),
(31, 3, 9, '2019-12-29 22:10:56', '2019-12-29 22:10:56'),
(32, 3, 2, '2019-12-29 22:10:56', '2019-12-29 22:10:56'),
(33, 4, 8, '2019-12-29 22:11:21', '2019-12-29 22:11:21'),
(34, 4, 1, '2019-12-29 22:11:21', '2019-12-29 22:11:21'),
(35, 4, 9, '2019-12-29 22:11:21', '2019-12-29 22:11:21'),
(36, 5, 2, '2019-12-29 22:11:42', '2019-12-29 22:11:42'),
(37, 5, 1, '2019-12-29 22:11:42', '2019-12-29 22:11:42'),
(38, 5, 9, '2019-12-29 22:11:42', '2019-12-29 22:11:42');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2014_10_12_100000_create_users_table', 1),
(3, '2019_07_21_004611_create_permission_tables', 1),
(4, '2019_07_21_032345_create_categorias_table', 1),
(5, '2019_07_21_032413_create_establecimientos_table', 1),
(6, '2019_07_21_032414_create_productos_table', 1),
(7, '2019_07_21_032427_create_publicidads_table', 1),
(8, '2019_07_21_032449_create_sugerencias_table', 1),
(9, '2019_07_21_032500_create_favoritos_table', 1),
(10, '2019_07_21_052924_create_favorito_detalles_table', 1),
(11, '2019_07_21_053053_create_publicidad_detalles_table', 1),
(12, '2019_07_21_053112_create_sugerencia_detalles_table', 1),
(13, '2019_07_25_202021_add_firebase_token_column_users_table', 1),
(14, '2019_07_25_231042_add_linkimagen_establecimientos_table', 1),
(15, '2019_07_26_145228_add_establecimiento_id_users_table', 1),
(16, '2019_08_13_013324_add_columns_establecimiento_table', 1),
(17, '2019_08_13_032238_add_columns_users_table', 1),
(18, '2019_08_13_213620_create_direccions_table', 1),
(19, '2019_08_13_213621_create_ordens_table', 1),
(20, '2019_08_13_213623_create_pedidos_table', 1),
(21, '2019_08_13_213659_create_pedido_detalles_table', 1),
(22, '2019_08_13_213741_create_carritos_table', 1),
(23, '2019_08_13_213742_create_pedido_carritos_table', 1),
(24, '2019_08_13_213814_create_pedido_carrito_detalles_table', 1),
(25, '2019_08_31_063638_add_column_cantidad_carritos_table', 1),
(26, '2019_08_31_194739_delete_columns_pedido_carritos', 1),
(27, '2019_08_31_202106_add_column_cantidad_pedido_carritos_detalles_table', 1),
(28, '2019_09_02_043235_make_columns_nullable_establecimientos', 1),
(29, '2019_09_04_185214_add_columns_direccions_table', 1),
(30, '2019_09_05_000326_add_selected_column_direccions_table', 1),
(31, '2019_10_14_071826_delete_column_catid_product', 1),
(32, '2019_10_14_071934_add_column_catid_estab', 1),
(33, '2019_10_14_111121_create_search_table', 1),
(34, '2019_10_31_171632_add__cant__column__ordens', 1),
(35, '2019_10_31_172011_add__cant__column__pedido__detalle', 1),
(36, '2019_10_31_182230_make__tiempo__repartidor_nullable', 1),
(37, '2019_11_01_170017_add__codigo__column__ordens', 1),
(38, '2019_11_03_051848_create_table_repartidor_estado', 1),
(39, '2019_11_03_052111_create_table_repartidor_estado_user', 1),
(40, '2019_11_04_212657_add_time_columns_orden', 1),
(41, '2019_11_22_195525_add_column_link_imagen_user_table', 1),
(42, '2019_11_24_181550_make_repartidor_id_column_nullable_ordens', 1),
(43, '2019_12_08_051541_drop__icon_link__column__on__category', 2),
(44, '2019_12_14_163358_add_categoria_id_column_favorito_detalles', 3),
(45, '2019_12_23_145820_add_lastname_column_user_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(2, 'App\\User', 3),
(2, 'App\\User', 4),
(2, 'App\\User', 13),
(2, 'App\\User', 14),
(2, 'App\\User', 17),
(2, 'App\\User', 18),
(2, 'App\\User', 19),
(2, 'App\\User', 20),
(2, 'App\\User', 21),
(2, 'App\\User', 22),
(2, 'App\\User', 23),
(2, 'App\\User', 24),
(2, 'App\\User', 25),
(2, 'App\\User', 26),
(2, 'App\\User', 27),
(2, 'App\\User', 28),
(2, 'App\\User', 30),
(2, 'App\\User', 31),
(2, 'App\\User', 32),
(2, 'App\\User', 33),
(2, 'App\\User', 34),
(2, 'App\\User', 35),
(2, 'App\\User', 36),
(2, 'App\\User', 37),
(2, 'App\\User', 38),
(2, 'App\\User', 39),
(2, 'App\\User', 40),
(2, 'App\\User', 41),
(2, 'App\\User', 42),
(2, 'App\\User', 43),
(2, 'App\\User', 44),
(2, 'App\\User', 45),
(2, 'App\\User', 46),
(2, 'App\\User', 47),
(2, 'App\\User', 48),
(2, 'App\\User', 49),
(2, 'App\\User', 50),
(2, 'App\\User', 51),
(2, 'App\\User', 52),
(2, 'App\\User', 53),
(2, 'App\\User', 54),
(2, 'App\\User', 55),
(3, 'App\\User', 12),
(3, 'App\\User', 15),
(3, 'App\\User', 16),
(5, 'App\\User', 56),
(6, 'App\\User', 11);

-- --------------------------------------------------------

--
-- Table structure for table `ordens`
--

CREATE TABLE `ordens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `costo_delivery` double(8,2) DEFAULT NULL,
  `estado` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tiempo` int(11) DEFAULT NULL,
  `subtotal` double(8,2) NOT NULL,
  `total` double(8,2) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `repartidor_id` bigint(20) UNSIGNED DEFAULT NULL,
  `direccion_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cant` int(11) DEFAULT NULL,
  `codigo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tiempoEstabs` int(11) DEFAULT NULL,
  `tiempoDel` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ordens`
--

INSERT INTO `ordens` (`id`, `costo_delivery`, `estado`, `tiempo`, `subtotal`, `total`, `user_id`, `repartidor_id`, `direccion_id`, `created_at`, `updated_at`, `cant`, `codigo`, `tiempoEstabs`, `tiempoDel`) VALUES
(1, NULL, 'procesando', 0, 10.00, 10.00, 4, NULL, 1, '2019-12-08 21:58:00', '2019-12-08 21:58:04', 1, 'pronto-674-08-Dec-2019', NULL, NULL),
(2, 2.50, 'DR', 30, 10.00, 12.50, 13, 11, 3, '2019-12-08 22:33:53', '2019-12-09 00:38:52', 1, 'pronto-1348-08-Dec-2019', 20, 10),
(3, 2.50, 'cancelado', 25, 15.00, 17.50, 14, 11, 4, '2019-12-21 16:17:36', '2019-12-21 21:07:17', 2, 'pronto-2022-21-Dec-2019', 10, 15),
(4, NULL, 'procesando', 0, 15.00, 15.00, 53, NULL, 5, '2019-12-29 06:58:04', '2019-12-29 06:58:08', 2, 'pronto-2696-29-Dec-2019', NULL, NULL),
(5, NULL, 'procesando', 0, 15.00, 15.00, 51, NULL, 6, '2019-12-29 07:05:03', '2019-12-29 07:05:07', 2, 'pronto-3370-29-Dec-2019', NULL, NULL),
(6, NULL, 'procesando', 0, 20.00, 20.00, 54, NULL, 7, '2019-12-29 16:23:19', '2019-12-29 16:23:27', 2, 'pronto-4044-29-Dec-2019', NULL, NULL),
(7, NULL, 'procesando', 0, 25.00, 25.00, 55, NULL, 8, '2019-12-29 16:28:39', '2019-12-29 16:28:47', 3, 'pronto-4718-29-Dec-2019', NULL, NULL),
(8, NULL, 'procesando', 0, 15.00, 15.00, 51, NULL, 6, '2019-12-30 03:55:24', '2019-12-30 03:55:32', 2, 'pronto-5392-29-Dec-2019', NULL, NULL),
(9, 2.50, 'cancelado', 40, 15.00, 17.50, 51, 11, 6, '2019-12-31 04:57:28', '2020-01-15 07:00:02', 2, 'pronto-6066-30-Dec-2019', 20, 20),
(10, 2.50, 'completo', 35, 5.00, 7.50, 51, 11, 6, '2019-12-31 05:21:20', '2019-12-31 05:44:48', 1, 'pronto-6740-30-Dec-2019', 20, 15);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pedidos`
--

CREATE TABLE `pedidos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subtotal` double(8,2) NOT NULL,
  `total` double(8,2) NOT NULL,
  `iva` double(8,2) NOT NULL,
  `estado` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tiempo` int(11) DEFAULT NULL,
  `establecimiento_id` bigint(20) UNSIGNED NOT NULL,
  `orden_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pedidos`
--

INSERT INTO `pedidos` (`id`, `subtotal`, `total`, `iva`, `estado`, `tiempo`, `establecimiento_id`, `orden_id`, `created_at`, `updated_at`) VALUES
(1, 10.00, 10.00, 0.00, 'procesando', 0, 3, 1, '2019-12-08 21:58:00', '2019-12-08 21:58:00'),
(2, 10.00, 10.00, 0.00, 'TS', 20, 3, 2, '2019-12-08 22:33:53', '2019-12-08 22:34:11'),
(3, 15.00, 15.00, 0.00, 'cancelado', 10, 3, 3, '2019-12-21 16:17:36', '2019-12-21 21:07:17'),
(4, 15.00, 15.00, 0.00, 'procesando', 0, 3, 4, '2019-12-29 06:58:04', '2019-12-29 06:58:04'),
(5, 15.00, 15.00, 0.00, 'procesando', 0, 3, 5, '2019-12-29 07:05:03', '2019-12-29 07:05:03'),
(6, 10.00, 10.00, 0.00, 'procesando', 0, 3, 6, '2019-12-29 16:23:19', '2019-12-29 16:23:19'),
(7, 10.00, 10.00, 0.00, 'procesando', 0, 4, 6, '2019-12-29 16:23:19', '2019-12-29 16:23:19'),
(8, 15.00, 15.00, 0.00, 'procesando', 0, 3, 7, '2019-12-29 16:28:39', '2019-12-29 16:28:39'),
(9, 10.00, 10.00, 0.00, 'procesando', 0, 4, 7, '2019-12-29 16:28:39', '2019-12-29 16:28:39'),
(10, 10.00, 10.00, 0.00, 'procesando', 0, 3, 8, '2019-12-30 03:55:24', '2019-12-30 03:55:24'),
(11, 5.00, 5.00, 0.00, 'procesando', 0, 5, 8, '2019-12-30 03:55:24', '2019-12-30 03:55:24'),
(12, 15.00, 15.00, 0.00, 'cancelado', 20, 3, 9, '2019-12-31 04:57:28', '2020-01-15 07:00:02'),
(13, 5.00, 5.00, 0.00, 'completo', 20, 3, 10, '2019-12-31 05:21:20', '2019-12-31 05:44:49');

-- --------------------------------------------------------

--
-- Table structure for table `pedido_carritos`
--

CREATE TABLE `pedido_carritos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subtotal` double(8,2) NOT NULL,
  `estado` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tiempo` int(11) DEFAULT NULL,
  `establecimiento_id` bigint(20) UNSIGNED NOT NULL,
  `carrito_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pedido_carritos`
--

INSERT INTO `pedido_carritos` (`id`, `subtotal`, `estado`, `tiempo`, `establecimiento_id`, `carrito_id`, `created_at`, `updated_at`) VALUES
(2, 10.00, NULL, NULL, 3, 1, '2019-12-08 21:59:27', '2019-12-08 21:59:27'),
(4, 10.00, NULL, NULL, 3, 5, '2019-12-08 22:51:03', '2019-12-08 22:51:03'),
(6, 15.00, NULL, NULL, 3, 11, '2019-12-22 13:53:56', '2019-12-22 13:53:57'),
(7, 10.00, NULL, NULL, 4, 11, '2019-12-22 13:53:57', '2019-12-22 13:53:57'),
(8, 30.00, NULL, NULL, 3, 12, '2019-12-22 13:56:32', '2019-12-22 13:56:32'),
(9, 20.00, NULL, NULL, 4, 12, '2019-12-22 13:56:32', '2019-12-22 13:56:32'),
(10, 10.00, NULL, NULL, 3, 13, '2019-12-22 14:01:17', '2019-12-22 14:01:17'),
(11, 20.00, NULL, NULL, 3, 14, '2019-12-22 14:04:33', '2019-12-22 14:04:33'),
(12, 40.00, NULL, NULL, 3, 16, '2019-12-22 22:34:04', '2019-12-22 22:34:12'),
(13, 30.00, NULL, NULL, 3, 17, '2019-12-22 22:36:47', '2019-12-22 22:36:48'),
(14, 20.00, NULL, NULL, 3, 18, '2019-12-22 22:39:42', '2019-12-22 22:39:42'),
(15, 20.00, NULL, NULL, 4, 18, '2019-12-22 22:39:42', '2019-12-22 22:39:42'),
(16, 30.00, NULL, NULL, 3, 19, '2019-12-22 22:41:35', '2019-12-22 22:41:35'),
(17, 10.00, NULL, NULL, 3, 20, '2019-12-22 22:45:52', '2019-12-22 22:45:52'),
(18, 20.00, NULL, NULL, 3, 21, '2019-12-22 22:47:17', '2019-12-22 22:47:17'),
(19, 20.00, NULL, NULL, 4, 21, '2019-12-22 22:47:17', '2019-12-22 22:47:17'),
(20, 15.00, NULL, NULL, 3, 22, '2019-12-22 22:50:08', '2019-12-22 22:50:08'),
(21, 20.00, NULL, NULL, 3, 23, '2019-12-23 01:02:20', '2019-12-23 01:02:20'),
(22, 320.00, NULL, NULL, 3, 24, '2019-12-23 01:09:13', '2019-12-23 04:27:21'),
(23, 120.00, NULL, NULL, 4, 24, '2019-12-23 01:13:10', '2019-12-23 04:27:22'),
(24, 240.00, NULL, NULL, 3, 28, '2019-12-23 03:16:42', '2019-12-23 03:21:10'),
(25, 20.00, NULL, NULL, 3, 29, '2019-12-23 03:21:48', '2019-12-23 03:22:21'),
(26, 10.00, NULL, NULL, 3, 30, '2019-12-23 04:33:13', '2019-12-23 04:33:13'),
(27, 40.00, NULL, NULL, 4, 31, '2019-12-23 06:13:51', '2019-12-23 06:14:57'),
(28, 40.00, NULL, NULL, 3, 32, '2019-12-23 06:17:57', '2019-12-23 06:19:15'),
(29, 15.00, NULL, NULL, 3, 33, '2019-12-23 06:33:56', '2019-12-23 06:33:57'),
(30, 20.00, NULL, NULL, 3, 34, '2019-12-23 06:37:11', '2019-12-23 06:58:23'),
(31, 10.00, NULL, NULL, 5, 34, '2019-12-23 06:41:33', '2019-12-23 06:41:33'),
(32, 15.00, NULL, NULL, 5, 37, '2019-12-24 18:46:56', '2019-12-24 18:50:22'),
(33, 10.00, NULL, NULL, 3, 37, '2019-12-24 18:46:56', '2019-12-24 18:46:56'),
(36, 5.00, NULL, NULL, 3, 44, '2019-12-29 10:03:42', '2019-12-29 10:03:42'),
(45, 15.00, NULL, NULL, 3, 52, '2019-12-31 05:58:23', '2019-12-31 05:58:36'),
(46, 5.00, NULL, NULL, 5, 52, '2019-12-31 23:49:51', '2019-12-31 23:49:51');

-- --------------------------------------------------------

--
-- Table structure for table `pedido_carrito_detalles`
--

CREATE TABLE `pedido_carrito_detalles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pedido_carrito_id` bigint(20) UNSIGNED NOT NULL,
  `producto_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cant` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pedido_carrito_detalles`
--

INSERT INTO `pedido_carrito_detalles` (`id`, `pedido_carrito_id`, `producto_id`, `created_at`, `updated_at`, `cant`) VALUES
(2, 2, 1, '2019-12-08 21:59:27', '2019-12-08 21:59:27', 1),
(4, 4, 1, '2019-12-08 22:51:03', '2019-12-08 22:51:03', 1),
(7, 6, 1, '2019-12-22 13:53:56', '2019-12-22 13:53:56', 1),
(8, 6, 2, '2019-12-22 13:53:57', '2019-12-22 13:53:57', 1),
(9, 7, 8, '2019-12-22 13:53:57', '2019-12-22 13:53:57', 1),
(10, 8, 1, '2019-12-22 13:56:32', '2019-12-22 13:56:32', 2),
(11, 8, 2, '2019-12-22 13:56:32', '2019-12-22 13:56:32', 2),
(12, 9, 8, '2019-12-22 13:56:32', '2019-12-22 13:56:32', 2),
(13, 10, 1, '2019-12-22 14:01:17', '2019-12-22 14:01:17', 1),
(14, 11, 1, '2019-12-22 14:04:33', '2019-12-22 14:04:33', 2),
(15, 12, 1, '2019-12-22 22:34:04', '2019-12-22 22:34:04', 2),
(16, 12, 2, '2019-12-22 22:34:05', '2019-12-22 22:34:12', 4),
(17, 13, 1, '2019-12-22 22:36:47', '2019-12-22 22:36:47', 2),
(18, 13, 2, '2019-12-22 22:36:48', '2019-12-22 22:36:48', 2),
(19, 14, 1, '2019-12-22 22:39:42', '2019-12-22 22:39:42', 2),
(20, 15, 8, '2019-12-22 22:39:42', '2019-12-22 22:39:42', 2),
(21, 16, 1, '2019-12-22 22:41:35', '2019-12-22 22:41:35', 2),
(22, 16, 2, '2019-12-22 22:41:35', '2019-12-22 22:41:35', 2),
(23, 17, 1, '2019-12-22 22:45:52', '2019-12-22 22:45:52', 1),
(24, 18, 1, '2019-12-22 22:47:17', '2019-12-22 22:47:17', 2),
(25, 19, 8, '2019-12-22 22:47:17', '2019-12-22 22:47:17', 2),
(26, 20, 2, '2019-12-22 22:50:08', '2019-12-22 22:50:08', 1),
(27, 20, 1, '2019-12-22 22:50:08', '2019-12-22 22:50:08', 1),
(28, 21, 1, '2019-12-23 01:02:20', '2019-12-23 01:02:20', 2),
(29, 22, 1, '2019-12-23 01:09:13', '2019-12-23 04:27:21', 24),
(30, 22, 2, '2019-12-23 01:11:42', '2019-12-23 04:27:21', 16),
(31, 23, 8, '2019-12-23 01:13:10', '2019-12-23 04:27:22', 12),
(32, 24, 1, '2019-12-23 03:16:42', '2019-12-23 03:21:09', 16),
(33, 24, 2, '2019-12-23 03:16:43', '2019-12-23 03:21:10', 16),
(34, 25, 2, '2019-12-23 03:21:48', '2019-12-23 03:22:21', 4),
(35, 26, 1, '2019-12-23 04:33:13', '2019-12-23 04:33:13', 1),
(36, 27, 8, '2019-12-23 06:13:51', '2019-12-23 06:14:57', 4),
(37, 28, 1, '2019-12-23 06:17:57', '2019-12-23 06:19:15', 4),
(38, 29, 1, '2019-12-23 06:33:56', '2019-12-23 06:33:56', 1),
(39, 29, 2, '2019-12-23 06:33:57', '2019-12-23 06:33:57', 1),
(40, 30, 1, '2019-12-23 06:37:11', '2019-12-23 06:37:11', 1),
(41, 31, 9, '2019-12-23 06:41:33', '2019-12-23 06:41:33', 2),
(42, 30, 2, '2019-12-23 06:58:23', '2019-12-23 06:58:23', 2),
(43, 32, 9, '2019-12-24 18:46:56', '2019-12-24 18:50:22', 3),
(44, 33, 1, '2019-12-24 18:46:56', '2019-12-24 18:46:56', 1),
(49, 36, 2, '2019-12-29 10:03:42', '2019-12-29 10:03:42', 1),
(60, 45, 1, '2019-12-31 05:58:23', '2019-12-31 05:58:23', 1),
(61, 45, 2, '2019-12-31 05:58:36', '2019-12-31 05:58:36', 1),
(62, 46, 9, '2019-12-31 23:49:51', '2019-12-31 23:49:51', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pedido_detalles`
--

CREATE TABLE `pedido_detalles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pedido_id` bigint(20) UNSIGNED NOT NULL,
  `producto_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cant` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pedido_detalles`
--

INSERT INTO `pedido_detalles` (`id`, `pedido_id`, `producto_id`, `created_at`, `updated_at`, `cant`) VALUES
(1, 1, 1, '2019-12-08 21:58:00', '2019-12-08 21:58:00', 1),
(2, 2, 1, '2019-12-08 22:33:53', '2019-12-08 22:33:53', 1),
(3, 3, 1, '2019-12-21 16:17:36', '2019-12-21 16:17:36', 1),
(4, 3, 2, '2019-12-21 16:17:36', '2019-12-21 16:17:36', 1),
(5, 4, 1, '2019-12-29 06:58:04', '2019-12-29 06:58:04', 1),
(6, 4, 2, '2019-12-29 06:58:04', '2019-12-29 06:58:04', 1),
(7, 5, 2, '2019-12-29 07:05:03', '2019-12-29 07:05:03', 1),
(8, 5, 1, '2019-12-29 07:05:03', '2019-12-29 07:05:03', 1),
(9, 6, 1, '2019-12-29 16:23:19', '2019-12-29 16:23:19', 1),
(10, 7, 8, '2019-12-29 16:23:19', '2019-12-29 16:23:19', 1),
(11, 8, 1, '2019-12-29 16:28:39', '2019-12-29 16:28:39', 1),
(12, 8, 2, '2019-12-29 16:28:39', '2019-12-29 16:28:39', 1),
(13, 9, 8, '2019-12-29 16:28:39', '2019-12-29 16:28:39', 1),
(14, 10, 1, '2019-12-30 03:55:24', '2019-12-30 03:55:24', 1),
(15, 11, 9, '2019-12-30 03:55:24', '2019-12-30 03:55:24', 1),
(16, 12, 2, '2019-12-31 04:57:28', '2019-12-31 04:57:28', 1),
(17, 12, 1, '2019-12-31 04:57:28', '2019-12-31 04:57:28', 1),
(18, 13, 2, '2019-12-31 05:21:20', '2019-12-31 05:21:20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `productos`
--

CREATE TABLE `productos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` longtext COLLATE utf8mb4_unicode_ci,
  `linkImagen` text COLLATE utf8mb4_unicode_ci,
  `establecimiento_id` bigint(20) UNSIGNED DEFAULT NULL,
  `precio` double(8,2) NOT NULL,
  `disponible` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `productos`
--

INSERT INTO `productos` (`id`, `nombre`, `descripcion`, `linkImagen`, `establecimiento_id`, `precio`, `disponible`, `created_at`, `updated_at`) VALUES
(1, 'Producto 1', 'Blusa dama', 'https://firebasestorage.googleapis.com/v0/b/prontoapp-5cec2.appspot.com/o/productos%2F1576368329102_Bitmap.png?alt=media&token=7c423100-0df5-4cfe-ba29-92e698cce9aa', 3, 10.00, 1, '2019-12-08 15:25:24', '2019-12-15 05:05:37'),
(2, 'Producto 2', ';lakdsjf;lasdjfa;lsdfjas;ldfjasl;dfjaskldjfhaskjdf', 'https://firebasestorage.googleapis.com/v0/b/prontoapp-5cec2.appspot.com/o/productos%2F1576307561266_Bitmap.png?alt=media&token=d6b9fb2a-d4ec-484b-a8f4-e1480162ebc3', 3, 5.00, 1, '2019-12-14 13:12:52', '2019-12-14 13:12:52'),
(8, 'Almuerzo', 'Arroz con menestra y carne', 'https://firebasestorage.googleapis.com/v0/b/prontoapp-5cec2.appspot.com/o/productos%2F1576961165557_Bitmap.png?alt=media&token=cc81f049-ae0f-4459-b5b1-7001f840e47d', 4, 10.00, 1, '2019-12-22 01:46:18', '2019-12-22 01:46:18'),
(9, 'Meriendas', 'Arroz con menestra y carne', 'https://firebasestorage.googleapis.com/v0/b/prontoapp-5cec2.appspot.com/o/productos%2F1576962883019_Bitmap.png?alt=media&token=b257f869-6854-495b-820c-601cca7bf762', 5, 5.00, 1, '2019-12-22 02:14:49', '2019-12-22 02:14:49');

-- --------------------------------------------------------

--
-- Table structure for table `publicidads`
--

CREATE TABLE `publicidads` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `orden` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `linkImagen` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `publicidads`
--

INSERT INTO `publicidads` (`id`, `nombre`, `orden`, `linkImagen`, `created_at`, `updated_at`) VALUES
(2, 'Publicidad 1', '1', 'https://firebasestorage.googleapis.com/v0/b/prontoapp-5cec2.appspot.com/o/publicidades%2F1577635955162_bitmap.png?alt=media&token=f07e3c2c-3067-4111-b231-d66bf0147419', '2019-12-29 22:12:43', '2019-12-29 22:12:43'),
(3, 'Publicidad 2', '1', 'https://firebasestorage.googleapis.com/v0/b/prontoapp-5cec2.appspot.com/o/publicidades%2F1577636034779_bitmap.png?alt=media&token=7ca875bb-2794-4061-8b41-bad5c661d149', '2019-12-29 22:14:03', '2019-12-29 22:14:03'),
(4, 'Publicidad 3', '2', 'https://firebasestorage.googleapis.com/v0/b/prontoapp-5cec2.appspot.com/o/publicidades%2F1577636063152_bitmap.png?alt=media&token=d9992b25-ea8e-4a1d-8186-a053ed786566', '2019-12-29 22:14:29', '2019-12-29 22:14:29'),
(5, 'Publicidad 4', '2', 'https://firebasestorage.googleapis.com/v0/b/prontoapp-5cec2.appspot.com/o/publicidades%2F1577636086878_bitmap.png?alt=media&token=e1e7828f-7b6a-451d-a324-5e06c4efdb0d', '2019-12-29 22:14:53', '2019-12-29 22:14:53'),
(6, 'Publicidad 5', '3', 'https://firebasestorage.googleapis.com/v0/b/prontoapp-5cec2.appspot.com/o/publicidades%2F1577636181739_bitmap.png?alt=media&token=8edf6b09-8308-45fe-9485-6041a029268c', '2019-12-29 22:16:27', '2019-12-29 22:16:27'),
(7, 'Publicidad 6', '3', 'https://firebasestorage.googleapis.com/v0/b/prontoapp-5cec2.appspot.com/o/publicidades%2F1577636204658_bitmap.png?alt=media&token=7588c20e-62b7-4e25-bf93-ffbe37064a6d', '2019-12-29 22:16:49', '2019-12-29 22:16:49');

-- --------------------------------------------------------

--
-- Table structure for table `publicidad_detalles`
--

CREATE TABLE `publicidad_detalles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `publicidad_id` bigint(20) UNSIGNED NOT NULL,
  `producto_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `publicidad_detalles`
--

INSERT INTO `publicidad_detalles` (`id`, `publicidad_id`, `producto_id`, `created_at`, `updated_at`) VALUES
(5, 2, 1, '2019-12-29 22:12:43', '2019-12-29 22:12:43'),
(6, 2, 2, '2019-12-29 22:12:43', '2019-12-29 22:12:43'),
(7, 3, 1, '2019-12-29 22:14:03', '2019-12-29 22:14:03'),
(8, 3, 8, '2019-12-29 22:14:03', '2019-12-29 22:14:03'),
(9, 4, 8, '2019-12-29 22:14:29', '2019-12-29 22:14:29'),
(10, 4, 9, '2019-12-29 22:14:29', '2019-12-29 22:14:29'),
(11, 5, 8, '2019-12-29 22:14:53', '2019-12-29 22:14:53'),
(12, 5, 9, '2019-12-29 22:14:53', '2019-12-29 22:14:53'),
(13, 6, 2, '2019-12-29 22:16:27', '2019-12-29 22:16:27'),
(14, 6, 8, '2019-12-29 22:16:27', '2019-12-29 22:16:27'),
(15, 7, 8, '2019-12-29 22:16:49', '2019-12-29 22:16:49'),
(16, 7, 9, '2019-12-29 22:16:49', '2019-12-29 22:16:49');

-- --------------------------------------------------------

--
-- Table structure for table `repartidor_estado`
--

CREATE TABLE `repartidor_estado` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `repartidor_estado`
--

INSERT INTO `repartidor_estado` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'disponible', '2019-12-06 14:25:59', '2019-12-06 14:25:59'),
(2, 'nodisponible', '2019-12-06 14:25:59', '2019-12-06 14:25:59');

-- --------------------------------------------------------

--
-- Table structure for table `repartidor_estado_user`
--

CREATE TABLE `repartidor_estado_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `estado_repartidor_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `repartidor_estado_user`
--

INSERT INTO `repartidor_estado_user` (`id`, `user_id`, `estado_repartidor_id`, `created_at`, `updated_at`) VALUES
(7, 11, 1, '2019-12-08 15:20:11', '2019-12-08 23:04:18');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'api', '2019-12-06 14:25:59', '2019-12-06 14:25:59'),
(2, 'client', 'api', '2019-12-06 14:25:59', '2019-12-06 14:25:59'),
(3, 'adminEstablecimiento', 'api', '2019-12-06 14:25:59', '2019-12-06 14:25:59'),
(4, 'secretarioEstablecimiento', 'api', '2019-12-06 14:25:59', '2019-12-06 14:25:59'),
(5, 'secretarioGlobal', 'api', '2019-12-06 14:25:59', '2019-12-06 14:25:59'),
(6, 'repartidor', 'api', '2019-12-06 14:25:59', '2019-12-06 14:25:59');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `search`
--

CREATE TABLE `search` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `palabra` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `search`
--

INSERT INTO `search` (`id`, `palabra`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'almu', 55, '2019-12-29 16:29:28', '2019-12-29 16:29:28'),
(2, 'flore', 55, '2019-12-29 16:29:59', '2019-12-29 16:29:59');

-- --------------------------------------------------------

--
-- Table structure for table `sugerencias`
--

CREATE TABLE `sugerencias` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `categoria_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sugerencia_detalles`
--

CREATE TABLE `sugerencia_detalles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sugerencia_id` bigint(20) UNSIGNED NOT NULL,
  `producto_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `firebase_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `establecimiento_id` bigint(20) UNSIGNED DEFAULT NULL,
  `direccion` text COLLATE utf8mb4_unicode_ci,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lng` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkImagen` longtext COLLATE utf8mb4_unicode_ci,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `firebase_token`, `establecimiento_id`, `direccion`, `lat`, `lng`, `telefono`, `linkImagen`, `lastname`) VALUES
(1, 'Admin', 'admin@admin.com', NULL, '$2y$10$zls8A/WN/xnivzWGHSnAGeGh2/NE9zyM60UZkak9vMYALJWPoeD3e', NULL, '2019-12-06 14:25:59', '2020-01-26 12:49:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Jorge', 'user3@gmail.com', NULL, '$2y$10$cXF.z1hCU26.7DOQUu/2UuQjeS4M7AiLhJ69TY7EEzUQ.M9uWR1Qi', NULL, '2019-12-08 07:08:00', '2019-12-08 07:08:00', NULL, NULL, NULL, NULL, NULL, '0946543435', NULL, NULL),
(4, 'Jorge', 'user5@gmail.com', NULL, '$2y$10$0X929bBfU4mnipg6UyWUM.p1izvRSHz76269Pv.dbpNIMvojf83G6', NULL, '2019-12-08 07:17:43', '2019-12-13 08:45:15', 'eifkDM7P2YU:APA91bGDqtK1UWeNCm9lsBj6pT3XKaMwepXo9kdoTOpxJpGcrKJ5yxdRNoCYzjDIv0s6SRmXAqsu0rxOQqi7Ri0iTSC3gUaDxqkfyOtlnGSLVJvD4xvd8CcifGvbO5r_9giD4BQyMvvQ', NULL, NULL, NULL, NULL, '0985435435', NULL, NULL),
(11, 'Ivan Fierro', 'ivan@gmail.com', NULL, '$2y$10$L80tVrxC8BIISZyo7WeGHujfGomIdU9EK9wc9MipjbcOQEJky0Yji', NULL, '2019-12-08 15:20:11', '2019-12-31 05:19:16', 'dUFOJY--wxs:APA91bGqJDMRUxFidqsgsfKYHGbBLXenq6NgV1nqeZGYEqHKgqGeCd-d7onIV7c-d59b5xWPJe3BvYdmbDKTfutObLpGEjq26JUKRZvzUpCHb_RG-T1EapziG05aK8gH7zycb4G25SgP', NULL, 'ghdfghdfghd', NULL, NULL, '035435350435', 'https://firebasestorage.googleapis.com/v0/b/prontoapp-5cec2.appspot.com/o/users%2F1575796776125_profile.jpg?alt=media&token=532ebdf9-646d-4141-a5a9-38fc2e4d4c03', NULL),
(12, 'Florentina', 'flore@gmail.com', NULL, '$2y$10$.kPHfzW21cCic70fonHnNekG0igzI0uuCr2tzH5JamrHrn8LJv5hy', NULL, '2019-12-08 15:22:32', '2019-12-31 05:33:57', 'fJWpO9KX0SI:APA91bGbgZW0zfYpX8yoLAH1Mqtf-Xoi9-HtNCZF-0mM_BwRCLfzNx2BryJdW4uDzwCOiK4dieNecmpjFMlw_YsZumAuXdbQFXDeqLFXIVhu1IdqDZADT3CD2YfLa58Ia9F38QLeEWBD', 3, 'rgewrgwergwer', NULL, NULL, '035435435', NULL, NULL),
(13, 'Jorge', 'user6@gmail.com', NULL, '$2y$10$aCfuK43JFQPy5IdSSyIa3uT4j07RyJ6jX1FSOgo4B8vO51KfgtVa.', NULL, '2019-12-08 22:00:16', '2019-12-09 00:38:33', 'd18z9WQFa5s:APA91bEwnCvTCSCYZ4kyvY-IemgjWi0xSxasph08WtrHDwuInmVfPanwGFlPlm1PXlJY0_llWk-5NoLk-w0kezmnYeDuFEIitEwBb0ru2y-HVvV5ULeQHX-SxzZ-yiSQP4B2-Snb2vOo', NULL, NULL, NULL, NULL, '0986543543', NULL, NULL),
(14, 'Jorge', 'user50@gmail.com', NULL, '$2y$10$qkvUuqPHwYM1WyCsYUItkOz8Isml9xD/ZzdqHyZr.fSkYPrN/oZMO', NULL, '2019-12-20 20:59:47', '2019-12-21 16:21:12', 'dW_t-h4eB5M:APA91bG6S0Z6uaa-HqPVWJZkP7toF8n5VHXoWfwTnnnHtg23cRrW7oj-M5OcTa64UA9gRci82iy8k8b35GVSGLNrVzugCjF_LEBm9jOhjWjDa3ctpNpkPrpYtD00Bxl53Nd82thIs-TZ', NULL, NULL, NULL, NULL, '0354343543', NULL, NULL),
(15, 'Jorge Fierro', 'jorge@gmail.com', NULL, '$2y$10$cvo/avm7pmYsMjVamGt9QO4zBKa4F/c0iKv8SSZbyONWqDZUhD.Q6', NULL, '2019-12-21 23:52:53', '2020-01-14 20:47:58', NULL, 4, 'sdgdsfgsdfgdsfg', NULL, NULL, '35435435435', NULL, NULL),
(16, 'Gabriel Arauz', 'gabriel@gmail.com', NULL, '$2y$10$AyJW0mBsGImaMScI8vFqHeGZbxltBvOvcPHA2xHaVcl5i419xrmqu', NULL, '2019-12-22 02:12:46', '2019-12-22 02:14:29', 'fAPhTkuxKBc:APA91bGvRlDBt5_Hm_8Ofmldzzmos9yz1kthXkvVG7e7B-VjXmUdrgLLVaHZeTKmDS3GHBtRytmMeyuwgI0iStoTOt-tUNMSUi_PKZu1p4RP-FwfttPkn0Qzl1gcH6a2sXczKqkylcQC', 5, 'adsfgdsfgdsgsdfgsd', NULL, NULL, '354354354354', NULL, NULL),
(17, 'Jorge', 'user100@gmail.com', NULL, '$2y$10$QHzv842qE2Q5NUutsXYo9.jE024dtARNjGc1MBOAMnHl3l.mGQz/a', NULL, '2019-12-22 13:45:29', '2019-12-22 13:45:29', NULL, NULL, NULL, NULL, NULL, '0343543543', NULL, NULL),
(18, 'Jorge', 'user200@gmail.com', NULL, '$2y$10$d5vyuZaEZ3l1XPwVvJwcPul5byWBqExRvDuGt10h2.M1X9fboc9Ie', NULL, '2019-12-22 13:48:51', '2019-12-22 13:48:51', NULL, NULL, NULL, NULL, NULL, '3543543543', NULL, NULL),
(19, 'Jorge', 'user40@gmail.com', NULL, '$2y$10$qSnFtrLb/U2utMhFiYgm.ududERooRxHQxGwjPeuHNWLkYdKMJXWG', NULL, '2019-12-22 13:51:00', '2019-12-22 13:51:00', NULL, NULL, NULL, NULL, NULL, '3543543543', NULL, NULL),
(20, 'Jorge', 'user234@gmail.com', NULL, '$2y$10$XWProa2kQ2Q5aHAnRwxay.v1/whFf0ylT8hB58hBtWXCNyZnKljeG', NULL, '2019-12-22 13:53:55', '2019-12-22 13:53:55', NULL, NULL, NULL, NULL, NULL, '1354354354', NULL, NULL),
(21, 'Jorge', 'user456@gmail.com', NULL, '$2y$10$gANfLapNryu8wyaxwLy0HeXotomX4Lc6CtoTVehBz84guKLUcNAxq', NULL, '2019-12-22 13:56:31', '2019-12-22 13:56:31', NULL, NULL, NULL, NULL, NULL, '3543543543', NULL, NULL),
(22, 'Jorge', 'user78@gmail.com', NULL, '$2y$10$B/O5YI/jrTrMZ1UoN946K.sXnaJRmNYYzNLnYsuXO3JuWGxFOX2Ry', NULL, '2019-12-22 14:01:16', '2019-12-22 14:01:16', NULL, NULL, NULL, NULL, NULL, '6874354354', NULL, NULL),
(23, 'Jorge', 'user43@gmail.com', NULL, '$2y$10$SYDbdMw58rPrEaHgGYJOleQdv8CFmSAm6ka5l1jvhLDNOCTpD7wm.', NULL, '2019-12-22 14:04:31', '2019-12-22 14:04:31', NULL, NULL, NULL, NULL, NULL, '5434354354', NULL, NULL),
(24, 'Jorge', 'user567@gmail.com', NULL, '$2y$10$9cBxfYYWkglF8e.Vxp9g6O.MN7n7tS.EN7VRGqU7xC7S2ud58PUtC', NULL, '2019-12-22 14:15:05', '2019-12-22 14:15:05', NULL, NULL, NULL, NULL, NULL, '2432435435', NULL, NULL),
(25, 'Jorge', 'user59@gmail.com', NULL, '$2y$10$IQ9N4zReUjI/FrzfJWjtr.oInwp.m1llGdkyHYypCZPLb63Al1X/y', NULL, '2019-12-22 22:34:03', '2019-12-22 22:34:03', NULL, NULL, NULL, NULL, NULL, '3543543543', NULL, NULL),
(26, 'Jorge', 'user10@gmail.com', NULL, '$2y$10$g0xOlsxY1yL4JbKXE1voxuUk.t.HwNLYzVHUZhykNtdzSsNMG1sd2', NULL, '2019-12-22 22:36:46', '2019-12-22 22:36:46', NULL, NULL, NULL, NULL, NULL, '3543543543', NULL, NULL),
(27, 'Jorge', 'user500@gmail.com', NULL, '$2y$10$ie1Jcw8iHEVy2PMrhEPFTuWNW0piNB.SGlclhK5DY6E08OQ8A6bfC', NULL, '2019-12-22 22:39:41', '2019-12-22 22:39:41', NULL, NULL, NULL, NULL, NULL, '3543543543', NULL, NULL),
(28, 'Jorge', 'user700@gmail.com', NULL, '$2y$10$ynUXIg9MZxrf5M2n9Y00heino4yFhbKWzXSgBsBX51.uDNgI7ztCi', NULL, '2019-12-22 22:41:33', '2019-12-22 22:41:33', NULL, NULL, NULL, NULL, NULL, '3843438453', NULL, NULL),
(30, 'Jorge', 'user900@gmail.com', NULL, '$2y$10$DRehX.HA7hqsGebAa6qCNu5Ou5WzKCFGciO1vAOssiTf453Ur0c1a', NULL, '2019-12-22 22:45:51', '2019-12-22 22:45:51', NULL, NULL, NULL, NULL, NULL, '6434384368', NULL, NULL),
(31, 'Jorge', 'user1000@gmail.com', NULL, '$2y$10$n1uLop1TSDL79lL7qWwT0e4m9D3qV.szZaLlKToGRRHr/blcmoIAi', NULL, '2019-12-22 22:47:16', '2019-12-22 22:47:16', NULL, NULL, NULL, NULL, NULL, '6876843584', NULL, NULL),
(32, 'Jorge', 'user2000@gmail.com', NULL, '$2y$10$CZRkUCQ.yU65C.qugWyefuqnH0hs/eG8sdnJkidI.7RRFxStavM5m', NULL, '2019-12-22 22:50:06', '2019-12-22 22:50:06', NULL, NULL, NULL, NULL, NULL, '6876838743', NULL, NULL),
(33, 'Jorge', 'user450@gmail.com', NULL, '$2y$10$9EQ/rzkckFvA5oeT7mr5wuOUcR7gUop/xp5LfpXcB09L5YSGI52u6', NULL, '2019-12-23 01:02:19', '2019-12-23 01:02:19', NULL, NULL, NULL, NULL, NULL, '3543543543', NULL, NULL),
(34, 'Jorge', 'user750@gmail.com', NULL, '$2y$10$1ejjgJonm3XQdQULLZDQ2epZTEPUrTbha31Qhnnzk2hNp91IYqRtG', NULL, '2019-12-23 01:09:11', '2019-12-23 01:09:11', NULL, NULL, NULL, NULL, NULL, '6868343843', NULL, NULL),
(35, 'Jorge', 'user6000@gmail.com', NULL, '$2y$10$azVAnromfXJS8LcsYkYL1eGdTQNn27J6HHSDf8i9OWlq005HKGcoG', NULL, '2019-12-23 02:36:55', '2019-12-23 02:36:55', NULL, NULL, NULL, NULL, NULL, '8468438438', NULL, NULL),
(36, 'Jorge', 'user7000@gmail.com', NULL, '$2y$10$TcsarS8RksNOhZd5PahQWe2yfiPKdEXX5PxK2q2Y93kawvPPtvWTS', NULL, '2019-12-23 02:38:46', '2019-12-23 02:38:46', NULL, NULL, NULL, NULL, NULL, '8438438438', NULL, NULL),
(37, 'Jorge', 'user2300@gmail.com', NULL, '$2y$10$/JYgnYLJDcEabRzcyRv1QOkUcBBQvNc2uolLzLhh7PRJ.vJH21e/y', NULL, '2019-12-23 02:52:15', '2019-12-23 02:52:15', NULL, NULL, NULL, NULL, NULL, '6843843848', NULL, NULL),
(38, 'Jjorge', 'user3400@gmail.com', NULL, '$2y$10$OAoDbPnDdjMm764IKshSWezqNBoUX1w0Q2FFT3kE6jr1PgTQbxgdS', NULL, '2019-12-23 03:16:41', '2019-12-23 03:16:41', NULL, NULL, NULL, NULL, NULL, '3846343843', NULL, NULL),
(39, 'Jorge', 'user5600@gmail.com', NULL, '$2y$10$Kd4JPXBJKXhrsY96KoKj9.IAGYYRP0zQn67j01FfhfUbhKrZy9Ew.', NULL, '2019-12-23 03:21:47', '2019-12-23 03:21:47', NULL, NULL, NULL, NULL, NULL, '6876846385', NULL, NULL),
(40, 'Jorge', 'user1230@gmail.com', NULL, '$2y$10$oyfnToTJPSA..HC1HOkAC.KCGRfRqn.LwaupqCbz5ThjKQEWDCgzm', NULL, '2019-12-23 04:33:11', '2019-12-23 04:33:11', NULL, NULL, NULL, NULL, NULL, '3543543543', NULL, NULL),
(41, 'Jorge', 'user4500@gmail.com', NULL, '$2y$10$6JPY5w7k.AduT81YV.maS.H2XZM6/aH5NmNOnrqGvQB3lJCommgkG', NULL, '2019-12-23 06:13:50', '2019-12-23 06:13:50', NULL, NULL, NULL, NULL, NULL, '3654354385', NULL, NULL),
(42, 'Jorge', 'user3620@gmail.com', NULL, '$2y$10$DIoSwRfgZ7gaz1kNPBDg2.EYlXrcqRgeExDjV/uYSQH.q2HOSjstO', NULL, '2019-12-23 06:17:55', '2019-12-23 06:17:55', NULL, NULL, NULL, NULL, NULL, '3846834384', NULL, NULL),
(43, 'Jorge', 'user2340@gmail.com', NULL, '$2y$10$pW35kVqcGqrOG/cPFRpNxegVAxzIn6WLESnyDUHYke1WHM5o8CeuS', NULL, '2019-12-23 06:33:55', '2019-12-23 06:33:55', NULL, NULL, NULL, NULL, NULL, '9494949494', NULL, NULL),
(44, 'Jorge', 'user6700@gmail.com', NULL, '$2y$10$V.e1LHulloW6OSB9uUl3IuVFUDlBXtjKwqDiSIJLE3Id1xkJuJg4S', NULL, '2019-12-23 06:37:10', '2019-12-23 06:37:10', NULL, NULL, NULL, NULL, NULL, '8468468468', NULL, NULL),
(45, 'Jorge', 'user67800@gmail.com', NULL, '$2y$10$og0CQs0qBRbWz/pNDuYcGuRXjyWWA6sqm61LRucVObJiwslDl3mh6', NULL, '2019-12-23 20:12:34', '2019-12-23 20:12:34', NULL, NULL, NULL, NULL, NULL, '3846834384', NULL, NULL),
(46, 'Jorge', 'user687@gmail.com', NULL, '$2y$10$qYnB8dgswEGWW/jgW6YhCeWPbCD1yEO4G2B6boE/FLjwtUKFAmo4.', NULL, '2019-12-23 20:36:53', '2019-12-23 20:36:53', NULL, NULL, NULL, NULL, NULL, '4384386438', NULL, 'Fierro'),
(47, 'Jorge', 'user7650@gmail.com', NULL, '$2y$10$qY1Afu.0kDLKi86XAqO6T.f0o/8UgPWs1JsBeSkWx5jGdTLwlEOkC', NULL, '2019-12-24 18:46:55', '2019-12-25 02:05:10', 'comRqclpCTs:APA91bFSFa_MOSDLnQUW7sAT_JCPJ_7Y-C6ajaR-2Er4tDtEOu1gDqoRiIA7zm8qcIADEg3hAj9pZRGCgPVamjfLsM9FL98A8kAtrh99FFY5logBTXhV1uPHHInDAmjguWp32KZfTC0G', NULL, NULL, NULL, NULL, '0986465646', NULL, 'Fierro'),
(48, 'jorge', 'user7482929@gmail.com', NULL, '$2y$10$OXR87d6xWyrTbJdSjN8PIOcpvLPxfxBiZ8RHOYI2MoSHLvS0Em6WG', NULL, '2019-12-28 03:51:31', '2019-12-28 03:58:04', NULL, NULL, NULL, NULL, NULL, '0982922306', NULL, 'Fierro'),
(49, 'Jorge', 'user47391@gmail.com', NULL, '$2y$10$4wLcrJ0f.mSwmYcR79kPM.L3sgQnwMNPioZxyPPKR2NGTfHHXbloO', NULL, '2019-12-28 04:03:10', '2019-12-28 04:03:57', NULL, NULL, NULL, NULL, NULL, '0989860408', NULL, 'Fierro'),
(50, 'Jorge', 'user59292047@gmail.com', NULL, '$2y$10$ZtNrlB0ewock/7nlDmQNCuiKUarzHw3Estgmhg8DVidcAATK6Rm3S', NULL, '2019-12-28 04:08:33', '2019-12-28 04:08:34', 'c83SbRtrdp0:APA91bHs9poAjGuLtHevrnChWkMaKS8ep3gMdOunYm5TdcMnuNUqZTodcxaauH6-UNC4q9TG3wmkCgRuQYWqSL-B8YUXthWxdyyObzUVCZEwvLFnRTdKq3D4emWLXTEImSL7Q-h8_avY', NULL, NULL, NULL, NULL, '0989860408', NULL, 'Fierro'),
(51, 'gabriel', 'arauzcastillo593@gmail.com', NULL, '$2y$10$besd/LrlXp0SMphPT6NbY.w3ckwtyZ3YNHATR4k0DHKtS0uRUNcA2', NULL, '2019-12-28 23:07:30', '2020-01-24 21:26:41', 'fB0GUxYl6q8:APA91bFPTJ9niWipDTOUDwfpdZLrzUi6Nw959zd4_W4T4EA753Nn_2exs-SzRqOuWYQJM7k3-ZFkMzVI--dXPD4QWRXmwws885OoePc9sPOtF74Xs2EKvk6NZxdZAhV0F5NSj8tF4iXj', NULL, NULL, NULL, NULL, '0989860408', NULL, 'Arauz'),
(52, 'Jorge', 'user3564564@gmail.com', NULL, '$2y$10$GINaeAKIqzLIroP7cD9dAet/RhkKy/wx819HeLWTPk7ZFd4VgfY5.', NULL, '2019-12-29 03:21:27', '2019-12-29 03:40:55', NULL, NULL, NULL, NULL, NULL, '0986431005', NULL, 'Fierro'),
(53, 'Jorge', 'user8583929@gmail.com', NULL, '$2y$10$FMAzOZPny6CR36KaOHIvgeOVvQOrsz3L7VnJeQNAQUMFmneUCa3iC', NULL, '2019-12-29 06:56:59', '2019-12-29 06:57:00', 'dNwiLNwig5w:APA91bG2vFOb2FuEC-73inWlXqeenUvfCyDvSglZ-REfTJmWkS06eI9bTmYsDcGLG1hfax5ubQwjqJas-atnBBEuNivRCQjJE2Zm3oUpxmdZXEGFwk9fI8Zlx3l7TDhTMS9aP37jZAwD', NULL, NULL, NULL, NULL, '0986431005', NULL, 'Fierri'),
(54, 'jorge', 'user694949@gmai.com', NULL, '$2y$10$v6kV2jOnD5lQqtLzlJF8HOnEOCu4d0mpMW0l8X2hMsCuTZ9DH6jCa', NULL, '2019-12-29 16:06:15', '2019-12-29 16:26:32', NULL, NULL, NULL, NULL, NULL, '0986431005', NULL, 'fierro'),
(55, 'jorge', 'user95949493@gmail.com', NULL, '$2y$10$iLS6OaFhWdB8c9zPWGWgv.A8uNnNr.eGxp6ppjg6cCYWDOTmw9Wuq', NULL, '2019-12-29 16:27:37', '2019-12-29 16:32:28', NULL, NULL, NULL, NULL, NULL, '0986431005', NULL, 'fierro'),
(56, 'Moflentina', 'mofle@gmail.com', NULL, '$2y$10$NyDONBu6Sks04R749i.1xuMDdkmV6JXObumQDykPddkUp3VSIQiea', NULL, '2019-12-31 05:31:08', '2020-01-26 12:50:29', 'ed03YMbprJ8:APA91bGFZANByBsHrBMYcsTgbXK-yt7Y6DEgqHkQ2kLwBN-Yw7iSlzFByG-fXJ7D1g1Nw6yR8soVqoJyYLLdeISJEAUqdw0gzy42vvuD7kfVlZCXF4311Y8NhaLtpTSzVUq_9v1irteY', NULL, 'Jdjdjdjsjsjsjsjs', NULL, NULL, '38373838', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carritos`
--
ALTER TABLE `carritos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carritos_user_id_foreign` (`user_id`),
  ADD KEY `carritos_direccion_id_foreign` (`direccion_id`);

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `direccions`
--
ALTER TABLE `direccions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `direccions_user_id_foreign` (`user_id`);

--
-- Indexes for table `establecimientos`
--
ALTER TABLE `establecimientos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `establecimientos_categoria_id_foreign` (`categoria_id`);

--
-- Indexes for table `favoritos`
--
ALTER TABLE `favoritos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorito_detalles`
--
ALTER TABLE `favorito_detalles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `favorito_detalles_favorito_id_foreign` (`favorito_id`),
  ADD KEY `favorito_detalles_producto_id_foreign` (`producto_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `ordens`
--
ALTER TABLE `ordens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ordens_user_id_foreign` (`user_id`),
  ADD KEY `ordens_repartidor_id_foreign` (`repartidor_id`),
  ADD KEY `ordens_direccion_id_foreign` (`direccion_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pedidos_establecimiento_id_foreign` (`establecimiento_id`),
  ADD KEY `pedidos_orden_id_foreign` (`orden_id`);

--
-- Indexes for table `pedido_carritos`
--
ALTER TABLE `pedido_carritos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pedido_carritos_establecimiento_id_foreign` (`establecimiento_id`),
  ADD KEY `pedido_carritos_carrito_id_foreign` (`carrito_id`);

--
-- Indexes for table `pedido_carrito_detalles`
--
ALTER TABLE `pedido_carrito_detalles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pedido_carrito_detalles_pedido_carrito_id_foreign` (`pedido_carrito_id`),
  ADD KEY `pedido_carrito_detalles_producto_id_foreign` (`producto_id`);

--
-- Indexes for table `pedido_detalles`
--
ALTER TABLE `pedido_detalles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pedido_detalles_pedido_id_foreign` (`pedido_id`),
  ADD KEY `pedido_detalles_producto_id_foreign` (`producto_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productos_establecimiento_id_foreign` (`establecimiento_id`);

--
-- Indexes for table `publicidads`
--
ALTER TABLE `publicidads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `publicidad_detalles`
--
ALTER TABLE `publicidad_detalles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `publicidad_detalles_publicidad_id_foreign` (`publicidad_id`),
  ADD KEY `publicidad_detalles_producto_id_foreign` (`producto_id`);

--
-- Indexes for table `repartidor_estado`
--
ALTER TABLE `repartidor_estado`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `repartidor_estado_user`
--
ALTER TABLE `repartidor_estado_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `repartidor_estado_user_user_id_foreign` (`user_id`),
  ADD KEY `repartidor_estado_user_estado_repartidor_id_foreign` (`estado_repartidor_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `search`
--
ALTER TABLE `search`
  ADD PRIMARY KEY (`id`),
  ADD KEY `search_user_id_foreign` (`user_id`);

--
-- Indexes for table `sugerencias`
--
ALTER TABLE `sugerencias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sugerencias_categoria_id_foreign` (`categoria_id`);

--
-- Indexes for table `sugerencia_detalles`
--
ALTER TABLE `sugerencia_detalles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sugerencia_detalles_sugerencia_id_foreign` (`sugerencia_id`),
  ADD KEY `sugerencia_detalles_producto_id_foreign` (`producto_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_establecimiento_id_foreign` (`establecimiento_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carritos`
--
ALTER TABLE `carritos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `direccions`
--
ALTER TABLE `direccions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `establecimientos`
--
ALTER TABLE `establecimientos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `favoritos`
--
ALTER TABLE `favoritos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `favorito_detalles`
--
ALTER TABLE `favorito_detalles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `ordens`
--
ALTER TABLE `ordens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `pedido_carritos`
--
ALTER TABLE `pedido_carritos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `pedido_carrito_detalles`
--
ALTER TABLE `pedido_carrito_detalles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `pedido_detalles`
--
ALTER TABLE `pedido_detalles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `productos`
--
ALTER TABLE `productos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `publicidads`
--
ALTER TABLE `publicidads`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `publicidad_detalles`
--
ALTER TABLE `publicidad_detalles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `repartidor_estado`
--
ALTER TABLE `repartidor_estado`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `repartidor_estado_user`
--
ALTER TABLE `repartidor_estado_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `search`
--
ALTER TABLE `search`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sugerencias`
--
ALTER TABLE `sugerencias`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sugerencia_detalles`
--
ALTER TABLE `sugerencia_detalles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `carritos`
--
ALTER TABLE `carritos`
  ADD CONSTRAINT `carritos_direccion_id_foreign` FOREIGN KEY (`direccion_id`) REFERENCES `direccions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `carritos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `direccions`
--
ALTER TABLE `direccions`
  ADD CONSTRAINT `direccions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `establecimientos`
--
ALTER TABLE `establecimientos`
  ADD CONSTRAINT `establecimientos_categoria_id_foreign` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `favorito_detalles`
--
ALTER TABLE `favorito_detalles`
  ADD CONSTRAINT `favorito_detalles_favorito_id_foreign` FOREIGN KEY (`favorito_id`) REFERENCES `favoritos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `favorito_detalles_producto_id_foreign` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ordens`
--
ALTER TABLE `ordens`
  ADD CONSTRAINT `ordens_direccion_id_foreign` FOREIGN KEY (`direccion_id`) REFERENCES `direccions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ordens_repartidor_id_foreign` FOREIGN KEY (`repartidor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ordens_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pedidos`
--
ALTER TABLE `pedidos`
  ADD CONSTRAINT `pedidos_establecimiento_id_foreign` FOREIGN KEY (`establecimiento_id`) REFERENCES `establecimientos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pedidos_orden_id_foreign` FOREIGN KEY (`orden_id`) REFERENCES `ordens` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pedido_carritos`
--
ALTER TABLE `pedido_carritos`
  ADD CONSTRAINT `pedido_carritos_carrito_id_foreign` FOREIGN KEY (`carrito_id`) REFERENCES `carritos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pedido_carritos_establecimiento_id_foreign` FOREIGN KEY (`establecimiento_id`) REFERENCES `establecimientos` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pedido_carrito_detalles`
--
ALTER TABLE `pedido_carrito_detalles`
  ADD CONSTRAINT `pedido_carrito_detalles_pedido_carrito_id_foreign` FOREIGN KEY (`pedido_carrito_id`) REFERENCES `pedido_carritos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pedido_carrito_detalles_producto_id_foreign` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pedido_detalles`
--
ALTER TABLE `pedido_detalles`
  ADD CONSTRAINT `pedido_detalles_pedido_id_foreign` FOREIGN KEY (`pedido_id`) REFERENCES `pedidos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pedido_detalles_producto_id_foreign` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `productos_establecimiento_id_foreign` FOREIGN KEY (`establecimiento_id`) REFERENCES `establecimientos` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `publicidad_detalles`
--
ALTER TABLE `publicidad_detalles`
  ADD CONSTRAINT `publicidad_detalles_producto_id_foreign` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `publicidad_detalles_publicidad_id_foreign` FOREIGN KEY (`publicidad_id`) REFERENCES `publicidads` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `repartidor_estado_user`
--
ALTER TABLE `repartidor_estado_user`
  ADD CONSTRAINT `repartidor_estado_user_estado_repartidor_id_foreign` FOREIGN KEY (`estado_repartidor_id`) REFERENCES `repartidor_estado` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `repartidor_estado_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `search`
--
ALTER TABLE `search`
  ADD CONSTRAINT `search_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sugerencias`
--
ALTER TABLE `sugerencias`
  ADD CONSTRAINT `sugerencias_categoria_id_foreign` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sugerencia_detalles`
--
ALTER TABLE `sugerencia_detalles`
  ADD CONSTRAINT `sugerencia_detalles_producto_id_foreign` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sugerencia_detalles_sugerencia_id_foreign` FOREIGN KEY (`sugerencia_id`) REFERENCES `sugerencias` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_establecimiento_id_foreign` FOREIGN KEY (`establecimiento_id`) REFERENCES `establecimientos` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
