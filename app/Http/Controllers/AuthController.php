<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\User;
use App\Carrito;
use App\RepartidorEstadoUser;
use DB;
use App\Orden;
use App\FirebaseToken;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {

        $credentials = $request->only('email', 'password');

        // $user = User::where('email', 'user100@gmail.com')->first();
        // $user->password = \Hash::make('12345678');
        // $user->save();

        if ($token = $this->guard()->attempt($credentials)) {
            return $this->respondWithToken($token);
        }

        return response()->json(['error' => 'Unauthorized'], 401);
    }


    public function register(Request $request)
    {
        try {
            $user = User::create($request->all());

            if($request->roles[0]["name"] != null) {
                if($request->roles[0]["name"]=="repartidor"){
                    $user->assignRole($request->roles[0]["name"]);
                    $repestado= new RepartidorEstadoUser;
                    $repestado->user_id=$user->id;
                    $repestado->estado_repartidor_id=2;
                    $repestado->save();

                    $user->linkImagen = $request->linkImagen;
                    $user->update();
                }
                else{
                    $user->assignRole($request->roles[0]["name"]);
                }
                
            } else {
                $user->assignRole('client');

                $carrito = array('subtotal' => 0.0, 'total' => 0.0 , 'user_id'=>$user->id,'cant'=>0);
                Carrito::create($carrito);
            }
            // return response()->json(['data' => $user], 201);
            $credentials = $request->only('email', 'password');

            if ($token = $this->guard()->attempt($credentials)) {
                return $this->respondWithToken($token);
            }

            return response()->json(['error' => 'Unauthorized'], 401);
        } catch(Exception $e) {
            Log::useDailyFiles(storage_path().'/logs/err.log');
            Log::info($e->getMessage());
            return response()->json(['error' => 'no se pudo obtener registros problema: '.$e->getMessage()], 500);
        }
    }
    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user=$this->guard()->user();
        // return response()->json();

        $u=User::with('roles', 'establecimiento')->find($user->id);
        if($u->roles[0]->name=="repartidor"){

            $userstate = DB::table('repartidor_estado_user')
            ->join('repartidor_estado', 'repartidor_estado.id', 'repartidor_estado_user.estado_repartidor_id')
            ->select( 
                'repartidor_estado.nombre'
                )
            ->where('repartidor_estado_user.user_id', $u->id)
            ->first();

            return response()->json([
                'user' => $u,
                'estado'=>$userstate
            ]);
        }
        else{
            return response()->json([
                'user' => $u
            ]);
        }
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {

        $user=$this->guard()->user();

        $u=User::find($user->id);
        $fbTOK = FirebaseToken::where('user_id', $u->id)
        ->where('firebase_token',$request->firebase_token);
        $fbTOK->delete();

        $this->guard()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function logoutRepartidor()
    {

        try {
            $user=$this->guard()->user();

            $pedidos=Orden::where('repartidor_id', $user->id)
            ->where('ordens.estado','DRCA')
            ->orWhere('ordens.estado','DA')->get();

            if($pedidos->count())
            {
                return null;
            }
            else{
                $u=User::find($user->id);
                $u->firebase_token=null;
                $u->update();

                $rep=RepartidorEstadoUser::where('user_id',$user->id)->first();
                $rep->estado_repartidor_id=2;
                $rep->update();

                $this->guard()->logout();

                return response()->json(['message' => 'Successfully logged out']);
            }
            
        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }

        

        
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        $user=User::with('roles', 'establecimiento')->find(Auth::id());
        if($user->roles[0]->name=="repartidor"){

            $userstate = DB::table('repartidor_estado_user')
            ->join('repartidor_estado', 'repartidor_estado.id', 'repartidor_estado_user.estado_repartidor_id')
            ->select( 
                'repartidor_estado.nombre'
                )
            ->where('repartidor_estado_user.user_id', $user->id)
            ->first();

            return response()->json([
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => null,//$this->guard()->factory()->getTTL(),
                'user' => $user,
                'estado'=>$userstate
            ]);
        }
        else{
            return response()->json([
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => null,//$this->guard()->factory()->getTTL(),
                'user' => $user
            ]);
        }
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard();
    }
}
