<?php

namespace App\Http\Controllers;

use App\Orden;
use App\Pedido;
use App\User;
use App\PedidoDetalle;
use DB;
use Response;
use Illuminate\Http\Request;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use DateTime;
use App\Jobs\CancelOrder;
use Carbon\Carbon;
use App\FirebaseToken;
use DispatchesJobs;
use App\Jobs\CancelCancellation;
use App\Direccion;

class OrdenController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function getOrdenes($userId){
        
        try {
            $myArray = []; 

            // $orden2 = Orden::where('user_id', $userId);

            foreach(Orden::where('user_id', $userId)->get() as $ord)
            {

                
                $myArray2=[];

                $pedidos = DB::table('ordens')
                ->join('pedidos', 'ordens.id', 'pedidos.orden_id')
                ->join('establecimientos', 'establecimientos.id', 'pedidos.establecimiento_id')
                ->select( 
                    'pedidos.id',
                    'pedidos.subtotal',
                    'pedidos.total',
                    'pedidos.iva',
                    'pedidos.estado',
                    'establecimientos.nombre AS establecimiento',
                    'establecimientos.direccion AS direccion',
                    'establecimientos.lat AS lat',
                    'establecimientos.lng AS lng',
                    'pedidos.establecimiento_id',
                    'pedidos.orden_id',
                    'pedidos.tiempo'

                    )
                ->where('ordens.id',$ord->id)
                ->get();

                foreach($pedidos as $p){

                    $sub=DB::table('pedidos')
                    ->join('pedido_detalles', 'pedidos.id', 'pedido_detalles.pedido_id')
                    ->join('productos', 'productos.id', 'pedido_detalles.producto_id')
                    ->join('establecimientos', 'establecimientos.id', 'productos.establecimiento_id')
                    ->join('categorias', 'categorias.id', 'establecimientos.categoria_id')
                    ->select( 
                        'productos.id',
                        'productos.nombre',
                        'productos.linkImagen',
                        'productos.descripcion',
                        'productos.precio',
                        'productos.disponible',
                        'categorias.id AS categoria_id',
                        'categorias.nombre AS categoria',
                        'pedido_detalles.id AS pedido_id',
                        'pedido_detalles.cant AS cant' 
                    )
                    ->where('pedidos.orden_id',$ord->id)
                    ->where('pedidos.establecimiento_id',$p->establecimiento_id)->get();

                    $arrayTemp=array(
                        'id'=>$p->id,
                        'subtotal'=>$p->subtotal,
                        'total'=>$p->total,
                        'iva'=>$p->iva,
                        'estado'=>$p->estado,
                        'establecimiento'=>$p->establecimiento,
                        'establecimiento_id'=>$p->establecimiento_id,
                        'orden_id'=>$p->orden_id,
                        'tiempo'=>$p->tiempo,
                        'productos'=>$sub
                    );

                    array_push($myArray2, $arrayTemp);
                }

                $dir=DB::table('ordens')
                ->join('direccions','direccions.id','ordens.direccion_id')
                ->select( 
                    'direccions.direccion',
                    'direccions.lat',
                    'direccions.lng',
                    'direccions.id'
                )
                ->where('ordens.id',$ord->id)
                ->first();

                $ordTemp=array(
                    'total'=>$ord->total,
                    'user'=> User::select('name')->where('id',$ord->user_id)->first(),
                    'direccion'=>$dir,
                    'cant'=>$ord->cant,
                    'orden_id'=>$ord->id,
                    'estate'=>$ord->estado,
                    'orden_codigo'=>$ord->codigo,
                    'pedidos'=>$myArray2,
                    'tiempoEstabs'=>$ord->tiempoEstabs,
                    'costo_delivery'=>$ord->costo_delivery,
                    'tiempo'=>$ord->tiempo,
                    'subtotal'=>$ord->subtotal,
                    'tiempoDel'=>$ord->tiempoDel
                );

                array_push($myArray, $ordTemp);

            }

            return $myArray;
        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function getOrdenesDR($userId){
        
        try {
            $myArray = []; 

            // $orden2 = Orden::where('user_id', $userId);

            foreach(Orden::where('user_id', $userId)->where('estado', 'DR')->get() as $ord)
            {

                
                $myArray2=[];

                // $pedidos = DB::table('ordens')
                // ->join('pedidos', 'ordens.id', 'pedidos.orden_id')
                // ->join('establecimientos', 'establecimientos.id', 'pedidos.establecimiento_id')
                // ->select( 
                //     'pedidos.id',
                //     'pedidos.subtotal',
                //     'pedidos.total',
                //     'pedidos.iva',
                //     'pedidos.estado',
                //     'establecimientos.nombre AS establecimiento',
                //     'establecimientos.direccion AS direccion',
                //     'establecimientos.lat AS lat',
                //     'establecimientos.lng AS lng',
                //     'pedidos.establecimiento_id',
                //     'pedidos.orden_id',
                //     'pedidos.tiempo'

                //     )
                // ->where('ordens.id',$ord->id)
                // ->get();

                // foreach($pedidos as $p){

                //     $sub=DB::table('pedidos')
                //     ->join('pedido_detalles', 'pedidos.id', 'pedido_detalles.pedido_id')
                //     ->join('productos', 'productos.id', 'pedido_detalles.producto_id')
                //     ->join('establecimientos', 'establecimientos.id', 'productos.establecimiento_id')
                //     ->join('categorias', 'categorias.id', 'establecimientos.categoria_id')
                //     ->select( 
                //         'productos.id',
                //         'productos.nombre',
                //         'productos.linkImagen',
                //         'productos.descripcion',
                //         'productos.precio',
                //         'productos.disponible',
                //         'categorias.id AS categoria_id',
                //         'categorias.nombre AS categoria',
                //         'pedido_detalles.id AS pedido_id',
                //         'pedido_detalles.cant AS cant' 
                //     )
                //     ->where('pedidos.orden_id',$ord->id)
                //     ->where('pedidos.establecimiento_id',$p->establecimiento_id)->get();

                //     $arrayTemp=array(
                //         'id'=>$p->id,
                //         'subtotal'=>$p->subtotal,
                //         'total'=>$p->total,
                //         'iva'=>$p->iva,
                //         'estado'=>$p->estado,
                //         'establecimiento'=>$p->establecimiento,
                //         'establecimiento_id'=>$p->establecimiento_id,
                //         'orden_id'=>$p->orden_id,
                //         'tiempo'=>$p->tiempo,
                //         'productos'=>$sub
                //     );

                //     array_push($myArray2, $arrayTemp);
                // }

                // $dir=DB::table('ordens')
                // ->join('direccions','direccions.id','ordens.direccion_id')
                // ->select( 
                //     'direccions.direccion',
                //     'direccions.lat',
                //     'direccions.lng',
                //     'direccions.id'
                // )
                // ->where('ordens.id',$ord->id)
                // ->first();

                // $ordTemp=array(
                //     'total'=>$ord->total,
                //     'user'=> User::select('name')->where('id',$ord->user_id)->first(),
                //     'direccion'=>$dir,
                //     'cant'=>$ord->cant,
                //     'orden_id'=>$ord->id,
                //     'estate'=>$ord->estado,
                //     'orden_codigo'=>$ord->codigo,
                //     'pedidos'=>$myArray2,
                //     'tiempoEstabs'=>$ord->tiempoEstabs,
                //     'costo_delivery'=>$ord->costo_delivery,
                //     'tiempo'=>$ord->tiempo,
                //     'subtotal'=>$ord->subtotal,
                //     'tiempoDel'=>$ord->tiempoDel
                // );

                $ordTemp=array($this->getOrdenById($ord->id));

                array_push($myArray, $ordTemp);

            }

            // $orden=Orden::where('user_id', $userId)->where('estado', 'DR')->get();

            // if(!$orden->isEmpty()){
            //     return response()->json(['pendiente' => true]);
            // }
            // else{
            //     return response()->json(['pendiente' => false]);
            // }
            return $myArray;

            
        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }


    public function getOrdenById($ordenID){
        
        try {

            // $orden2 = Orden::where('user_id', $userId);
            $ord=Orden::where('id', $ordenID)->first();

            

                
            $products = DB::table('ordens')
            ->join('pedidos', 'ordens.id', 'pedidos.orden_id')
            ->join('pedido_detalles', 'pedidos.id', 'pedido_detalles.pedido_id')
            ->join('productos', 'productos.id', 'pedido_detalles.producto_id')
            ->join('establecimientos', 'establecimientos.id', 'productos.establecimiento_id')
            ->join('categorias', 'categorias.id', 'establecimientos.categoria_id')
            ->select( 
                'productos.id',
                'productos.nombre',
                'productos.linkImagen',
                'productos.descripcion',
                'productos.precio',
                'productos.disponible',
                'categorias.id AS categoria_id',
                'categorias.nombre AS categoria',
                'productos.establecimiento_id', 
                'establecimientos.nombre AS establecimiento',
                'pedido_detalles.id AS pedido_id',
                'pedido_detalles.cant AS cant' ,
                'ordens.updated_at'
                )
            ->where('ordens.id',$ord->id)->get();

            $date = new DateTime($ord->updated_at, new \DateTimeZone('GMT-5'));

            return array(
                'products'=>$products,
                'total'=>$ord->total,
                'subtotal'=>$ord->subtotal,
                'costo_delivery'=>$ord->costo_delivery,
                'user_id'=>$ord->user_id,
                'cant'=>$ord->cant,
                'orden_id'=>$ord->id,
                'estate'=>$ord->estado,
                'orden_codigo'=>$ord->codigo,
                'tiempo'=>$ord->tiempo,
                'updated_at'=>$date
            );
        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function getOrdenesTS(){
        
        try {
            $myArray = []; 

            foreach(
            Orden::where('estado', "TS")
            ->orWhere('estado', 'DR')
            ->orWhere('estado', 'DREP')
            ->orWhere('estado', 'DA')
            ->orWhere('estado', 'cancelado')
            ->orWhere('estado', 'completo')->get() as $ord)
            {
                $myArray2=[];

                $pedidos = DB::table('ordens')
                ->join('pedidos', 'ordens.id', 'pedidos.orden_id')
                ->join('establecimientos', 'establecimientos.id', 'pedidos.establecimiento_id')
                ->select( 
                    'pedidos.id',
                    'pedidos.subtotal',
                    'pedidos.total',
                    'pedidos.iva',
                    'pedidos.estado',
                    'establecimientos.nombre AS establecimiento',
                    'pedidos.establecimiento_id',
                    'pedidos.orden_id',
                    'pedidos.tiempo',
                    'establecimientos.direccion',
                    'establecimientos.lat',
                    'establecimientos.lng'
                    )
                ->where('ordens.id',$ord->id)->get();

                foreach($pedidos as $p){

                    $sub=DB::table('pedidos')
                    ->join('pedido_detalles', 'pedidos.id', 'pedido_detalles.pedido_id')
                    ->join('productos', 'productos.id', 'pedido_detalles.producto_id')
                    ->join('establecimientos', 'establecimientos.id', 'productos.establecimiento_id')
                    ->join('categorias', 'categorias.id', 'establecimientos.categoria_id')
                    ->select( 
                        'productos.id',
                        'productos.nombre',
                        'productos.linkImagen',
                        'productos.descripcion',
                        'productos.precio',
                        'productos.disponible',
                        'categorias.id AS categoria_id',
                        'categorias.nombre AS categoria',
                        'pedido_detalles.id AS pedido_id',
                        'pedido_detalles.cant AS cant' 
                    )
                    ->where('pedidos.orden_id',$ord->id)
                    ->where('pedidos.establecimiento_id',$p->establecimiento_id)->get();

                    $arrayTemp=array(
                        'id'=>$p->id,
                        'subtotal'=>$p->subtotal,
                        'total'=>$p->total,
                        'iva'=>$p->iva,
                        'estado'=>$p->estado,
                        'establecimiento'=>$p->establecimiento,
                        'establecimiento_id'=>$p->establecimiento_id,
                        'direccionEstab'=>$p->direccion,
                        'latEstab'=>$p->lat,
                        'lngEstab'=>$p->lng,
                        'orden_id'=>$p->orden_id,
                        'tiempo'=>$p->tiempo,
                        'productos'=>$sub
                    );

                    array_push($myArray2, $arrayTemp);

                }

                $dirEnvio=Direccion::where('id',$ord->direccion_id)->first();

                $ordTemp=array(
                    'total'=>$ord->total,
                    'user'=> User::where('id',$ord->user_id)->first(),
                    'cant'=>$ord->cant,
                    'orden_id'=>$ord->id,
                    'estate'=>$ord->estado,
                    'orden_codigo'=>$ord->codigo,
                    'pedidos'=>$myArray2,
                    'tiempoEstabs'=>$ord->tiempoEstabs,
                    'costo_delivery'=>$ord->costo_delivery,
                    'repartidor'=>User::where('id',$ord->repartidor_id)->first(),
                    'tiempo'=>$ord->tiempo,
                    'subtotal'=>$ord->subtotal,
                    'tiempoDel'=>$ord->tiempoDel,
                    'orden_creada'=>$ord->created_at,
                    'orden_actualizada'=>$ord->updated_at,
                    'direccionEnvio'=>$dirEnvio->direccion,
                    'latEnvio'=>$dirEnvio->lat,
                    'lngEnvio'=>$dirEnvio->lng
                );

                array_push($myArray, $ordTemp);

            }

            return $myArray;
        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function setTimePriceDelOrder(Request $request){
        try {
            $ord=Orden::where('id',$request->ordenId)->first();
            $ord->tiempoDel=$request->time;
            $ord->tiempo=$ord->tiempoEstabs+$request->time;
            $ord->repartidor_id=$request->delRep;
            $ord->costo_delivery=$request->delPrice;
            $ord->total=$ord->subtotal+$request->delPrice;
            $ord->estado="DR";
            $ord->update();


            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60*20);

            $notificationBuilder = new PayloadNotificationBuilder('¡Tu pedido fue procesado!');
            $notificationBuilder->setBody('Presiona aqui para revisar y confirmar tu pedido...')
                                ->setSound('default');

            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData([
                'pedido' => $this->getOrdenById($request->ordenId),
                'confirmation' => true,
                "forceStart"=> "1",
                "priority"=> "high",
            ]);

            $option = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();

            // $user=User::where('id',$ord->user_id)->first();

            // $users=User::where('id',$ord->user_id)->pluck('id')->toArray();
            $userTokens=FirebaseToken::where('user_id',$ord->user_id)->pluck('firebase_token')->toArray();

            // echo $user->firebase_token;
             if(count($userTokens)>0){

                // $token = $user->firebase_token;

                $downstreamResponse = FCM::sendTo($userTokens, $option, $notification, $data);
                $downstreamResponse->numberSuccess();
                $downstreamResponse->numberFailure();
                $downstreamResponse->numberModification();
            }

            $cancelorder = (new CancelOrder($ord))->delay(2100);
            $this->dispatch($cancelorder);
            
        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function acceptOrden(Request $request){
        try {
            $ord=Orden::where('id',$request->ordenId)->first();
            $ord->estado="DRCA";
            $ord->update();

            $user=User::where('id',$ord->repartidor_id)->first();

            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60*20);

            $notificationBuilder = new PayloadNotificationBuilder('¡Pedido Confirmado!');
            $notificationBuilder->setBody('Presiona aqui para revisar los detalles...')
                                ->setSound('default');

            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData(['a_data' => 'my_data']);

            $option = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();

            // echo $user->firebase_token;
             if($user->firebase_token){

                $token = $user->firebase_token;

                $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
                $downstreamResponse->numberSuccess();
                $downstreamResponse->numberFailure();
                $downstreamResponse->numberModification();
            }

            // $pedido_detalle=PedidoDetalle::where('pedido_id',$ord->id);

            foreach(Pedido::where('orden_id',$ord->id)->get() as $pd){
                $pd->estado="DRCA";
                $pd->update();
                
                // $user=User::where('establecimiento_id',$pd->establecimiento_id)->first();
                $users=User::where('establecimiento_id',$pd->establecimiento_id)->pluck('id')->toArray();

                $optionBuilder = new OptionsBuilder();
                $optionBuilder->setTimeToLive(60*20);
    
                $notificationBuilder = new PayloadNotificationBuilder('¡Orden Confirmada!');
                $notificationBuilder->setBody('')
                                    ->setSound('default');
    
                $dataBuilder = new PayloadDataBuilder();
                $dataBuilder->addData(['a_data' => 'my_data']);
    
                $option = $optionBuilder->build();
                $notification = $notificationBuilder->build();
                $data = $dataBuilder->build();
    
                // echo $user->firebase_token;
                $userTokens=FirebaseToken::whereIn('user_id',$users)->pluck('firebase_token')->toArray();

                if(count($userTokens)>0){
                    // $token = $user->firebase_token;
        
                    $downstreamResponse = FCM::sendTo($userTokens, $option, $notification, $data);
                    $downstreamResponse->numberSuccess();
                    $downstreamResponse->numberFailure();
                    $downstreamResponse->numberModification();
                }
            }


            
            
        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function setRepOnly(Request $request){
        try {
            $ord=Orden::where('id',$request->ordenId)->first();
            $ord->repartidor_id=$request->delRep;
            $ord->update();

            $user=User::where('id',$ord->repartidor_id)->first();

            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60*20);

            $notificationBuilder = new PayloadNotificationBuilder('my title');
            $notificationBuilder->setBody('Hello world')
                                ->setSound('default');

            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData(['a_data' => 'my_data']);

            $option = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();

            // echo $user->firebase_token;
             if($user->firebase_token){

                $token = $user->firebase_token;

                $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
                $downstreamResponse->numberSuccess();
                $downstreamResponse->numberFailure();
                $downstreamResponse->numberModification();
            }
            
            
        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function getOrdenesRepartidor(){
        
        try {
            $myArray = []; 

            foreach(Orden::where('estado', "TS")->orWhere('estado', 'DR')->get() as $ord)
            {
                $myArray2=[];

                $pedidos = DB::table('ordens')
                ->join('pedidos', 'ordens.id', 'pedidos.orden_id')
                ->join('establecimientos', 'establecimientos.id', 'pedidos.establecimiento_id')
                ->select( 
                    'pedidos.id',
                    'pedidos.subtotal',
                    'pedidos.total',
                    'pedidos.iva',
                    'pedidos.estado',
                    'establecimientos.nombre AS establecimiento',
                    'pedidos.establecimiento_id',
                    'pedidos.orden_id',
                    'pedidos.tiempo'

                    )
                ->where('ordens.id',$ord->id)->get();

                foreach($pedidos as $p){

                    $sub=DB::table('pedidos')
                    ->join('pedido_detalles', 'pedidos.id', 'pedido_detalles.pedido_id')
                    ->join('productos', 'productos.id', 'pedido_detalles.producto_id')
                    ->join('establecimientos', 'establecimientos.id', 'productos.establecimiento_id')
                    ->join('categorias', 'categorias.id', 'establecimientos.categoria_id')
                    ->select( 
                        'productos.id',
                        'productos.nombre',
                        'productos.linkImagen',
                        'productos.descripcion',
                        'productos.precio',
                        'productos.disponible',
                        'categorias.id AS categoria_id',
                        'categorias.nombre AS categoria',
                        'pedido_detalles.id AS pedido_id',
                        'pedido_detalles.cant AS cant' 
                    )
                    ->where('pedidos.orden_id',$ord->id)
                    ->where('pedidos.establecimiento_id',$p->establecimiento_id)->get();

                    $arrayTemp=array(
                        'id'=>$p->id,
                        'subtotal'=>$p->subtotal,
                        'total'=>$p->total,
                        'iva'=>$p->iva,
                        'estado'=>$p->estado,
                        'establecimiento'=>$p->establecimiento,
                        'establecimiento_id'=>$p->establecimiento_id,
                        'orden_id'=>$p->orden_id,
                        'tiempo'=>$p->tiempo,
                        'productos'=>$sub
                    );

                    array_push($myArray2, $arrayTemp);

                }

                $ordTemp=array(
                    'total'=>$ord->total,
                    'user'=> User::where('id',$ord->user_id)->first(),
                    'cant'=>$ord->cant,
                    'orden_id'=>$ord->id,
                    'estate'=>$ord->estado,
                    'orden_codigo'=>$ord->codigo,
                    'pedidos'=>$myArray2,
                    'tiempoEstabs'=>$ord->tiempoEstabs,
                    'costo_delivery'=>$ord->costo_delivery,
                    'tiempo'=>$ord->tiempo,
                    'subtotal'=>$ord->subtotal,
                    'tiempoDel'=>$ord->tiempoDel
                );

                array_push($myArray, $ordTemp);

            }

            return $myArray;
        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function getOrdenesByRepartidor($repId){
        
        try {
            $myArray = []; 

            foreach(Orden::where('repartidor_id', $repId)
            ->where('ordens.estado','DR')
            ->orWhere('ordens.estado','DRCA')
            ->orWhere('ordens.estado','DA')->get() as $ord)
            {
                $myArray2=[];

                $pedidos = DB::table('ordens')
                ->join('pedidos', 'ordens.id', 'pedidos.orden_id')
                ->join('establecimientos', 'establecimientos.id', 'pedidos.establecimiento_id')
                ->select( 
                    'pedidos.id',
                    'pedidos.subtotal',
                    'pedidos.total',
                    'pedidos.iva',
                    'pedidos.estado',
                    'establecimientos.nombre AS establecimiento',
                    'establecimientos.direccion AS direccion',
                    'establecimientos.lat AS lat',
                    'establecimientos.lng AS lng',
                    'pedidos.establecimiento_id',
                    'pedidos.orden_id',
                    'pedidos.tiempo'

                    )
                ->where('ordens.id',$ord->id)
                ->get();

                foreach($pedidos as $p){

                    $sub=DB::table('pedidos')
                    ->join('pedido_detalles', 'pedidos.id', 'pedido_detalles.pedido_id')
                    ->join('productos', 'productos.id', 'pedido_detalles.producto_id')
                    ->join('establecimientos', 'establecimientos.id', 'productos.establecimiento_id')
                    ->join('categorias', 'categorias.id', 'establecimientos.categoria_id')
                    ->select( 
                        'productos.id',
                        'productos.nombre',
                        'productos.linkImagen',
                        'productos.descripcion',
                        'productos.precio',
                        'productos.disponible',
                        'categorias.id AS categoria_id',
                        'categorias.nombre AS categoria',
                        'pedido_detalles.id AS pedido_id',
                        'pedido_detalles.cant AS cant' 
                    )
                    ->where('pedidos.orden_id',$ord->id)
                    ->where('pedidos.establecimiento_id',$p->establecimiento_id)->get();

                    $arrayTemp=array(
                        'id'=>$p->id,
                        'subtotal'=>$p->subtotal,
                        'total'=>$p->total,
                        'iva'=>$p->iva,
                        'estado'=>$p->estado,
                        'establecimiento'=>$p->establecimiento,
                        'establecimiento_id'=>$p->establecimiento_id,
                        'direccion_estab'=>$p->direccion,
                        'lat'=>$p->lat,
                        'lng'=>$p->lng,
                        'orden_id'=>$p->orden_id,
                        'tiempo'=>$p->tiempo,
                        'productos'=>$sub
                    );

                    array_push($myArray2, $arrayTemp);

                }

                $dir=DB::table('ordens')
                ->join('direccions','direccions.id','ordens.direccion_id')
                ->select( 
                    'direccions.direccion',
                    'direccions.lat',
                    'direccions.lng',
                    'direccions.id'
                )
                ->where('ordens.id',$ord->id)
                ->first();

                $ordTemp=array(
                    'total'=>$ord->total,
                    'user'=> User::select('name')->where('id',$ord->user_id)->first(),
                    'direccion'=>$dir,
                    'cant'=>$ord->cant,
                    'orden_id'=>$ord->id,
                    'estate'=>$ord->estado,
                    'orden_codigo'=>$ord->codigo,
                    'pedidos'=>$myArray2,
                    'tiempoEstabs'=>$ord->tiempoEstabs,
                    'costo_delivery'=>$ord->costo_delivery,
                    'tiempo'=>$ord->tiempo,
                    'subtotal'=>$ord->subtotal,
                    'tiempoDel'=>$ord->tiempoDel
                );

                array_push($myArray, $ordTemp);

            }

            return $myArray;
        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    // delveryAccepted(){

    // }

    // deliveryCanceled(){

    // }

    public function deliveryAccepted(Request $request){
        try {
            $ord=Orden::where('id',$request->ordenId)->first();
            $ord->estado="DRCA";
            $ord->update();

            $user=User::where('id',$ord->repartidor_id)->first();

            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60*20);

            $notificationBuilder = new PayloadNotificationBuilder('¡Nuevo Pedido!');
            $notificationBuilder->setBody('Presiona aquí para ver más información...')
                                ->setSound('default');

            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData(['a_data' => 'my_data']);

            $option = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();

            // echo $user->firebase_token;
            if($user->firebase_token){

                $token = $user->firebase_token;

                $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
                $downstreamResponse->numberSuccess();
                $downstreamResponse->numberFailure();
                $downstreamResponse->numberModification();
            }

            foreach(Pedido::where('orden_id',$ord->id)->get() as $pd){
                $pd->estado="DRCA";
                $pd->update();
                
                // $user=User::where('establecimiento_id',$pd->establecimiento_id)->first();
                $users=User::where('establecimiento_id',$pd->establecimiento_id)->pluck('id')->toArray();

                $optionBuilder = new OptionsBuilder();
                $optionBuilder->setTimeToLive(60*20);
    
                $notificationBuilder = new PayloadNotificationBuilder('¡Orden Confirmada!');
                $notificationBuilder->setBody('')
                                    ->setSound('default');
    
                $dataBuilder = new PayloadDataBuilder();
                $dataBuilder->addData(['a_data' => 'my_data']);
    
                $option = $optionBuilder->build();
                $notification = $notificationBuilder->build();
                $data = $dataBuilder->build();
    
                // echo $user->firebase_token;
                $userTokens=FirebaseToken::whereIn('user_id',$users)->pluck('firebase_token')->toArray();

                if(count($userTokens)>0){
                    // $token = $user->firebase_token;
        
                    $downstreamResponse = FCM::sendTo($userTokens, $option, $notification, $data);
                    $downstreamResponse->numberSuccess();
                    $downstreamResponse->numberFailure();
                    $downstreamResponse->numberModification();
                }
            }
            
        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function deliveryCanceled(Request $request){
        try {
            $codigo_orden;
            $ordenId=$request->ordenId;

            $ord=Orden::where('id',$ordenId)->first();
            $ord->estado="cancelado";
            $codigo_orden=$ord->codigo;
            $ord->update();


            foreach(Pedido::where('orden_id',$ord->id)->get() as $pc)
            {
                $pc->estado="cancelado";
                $pc->update();
                //Notificacion Repartidor

                // $user=User::where('establecimiento_id',$pc->establecimiento_id)->first();
                $users=User::where('establecimiento_id',$pc->establecimiento_id)->pluck('id')->toArray();

                $optionBuilder = new OptionsBuilder();
                $optionBuilder->setTimeToLive(60*20);

                $notificationBuilder = new PayloadNotificationBuilder('¡Pedido Cancelado!');
                $msg="El pedido $codigo_orden fue cancelado...";
                $notificationBuilder->setBody($msg)
                                    ->setSound('default');

                $dataBuilder = new PayloadDataBuilder();
                $dataBuilder->addData(['canceled' => 'my_data']);

                $option = $optionBuilder->build();
                $notification = $notificationBuilder->build();
                $data = $dataBuilder->build();
                
                $userTokens=FirebaseToken::whereIn('user_id',$users)->pluck('firebase_token')->toArray();

                // echo $user->firebase_token;
                if(count($userTokens)>0){

                    // $token = $user->firebase_token;

                    $downstreamResponse = FCM::sendTo($userTokens, $option, $notification, $data);
                    $downstreamResponse->numberSuccess();
                    $downstreamResponse->numberFailure();
                    $downstreamResponse->numberModification();
                }

                
            }   
            //Notificacion Administrador y su secretario

            // $user=User::where('id', 1)->first();

            // $users=User::where('establecimiento_id',$pc->establecimiento_id)->pluck('id')->toArray();
            
            $users = DB::table('users')
            ->join('model_has_roles', 'users.id', 'model_has_roles.model_id')
            ->select( 
                'users.id as id'
                )
            ->where('model_has_roles.role_id', 1)
            ->orWhere('model_has_roles.role_id', 5)
            ->pluck('id')->toArray();

            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60*20);

            $notificationBuilder = new PayloadNotificationBuilder('¡Pedido Cancelado!');
            $notificationBuilder->setBody("El pedido $codigo_orden fue cancelado...")
                                ->setSound('default');

            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData(['a_data' => 'my_data']);

            $option = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();

            // echo $user->firebase_token;
            $userTokens=FirebaseToken::whereIn('user_id',$users)->pluck('firebase_token')->toArray();

            if(count($userTokens)>0){

                // $token = $user->firebase_token;

                $downstreamResponse = FCM::sendTo($userTokens, $option, $notification, $data);
                $downstreamResponse->numberSuccess();
                $downstreamResponse->numberFailure();
                $downstreamResponse->numberModification();
            }         
        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }


    public function deliveryArrived(Request $request){
        try {
            $codigo_orden;
            $ord=Orden::where('id',$request->ordenId)->first();
            $ord->estado="DA";
            $codigo_orden=$ord->codigo;
            $ord->update();
            
            // $user=User::where('id',$ord->user_id)->first();
            $user=User::where('id',$ord->user_id)->pluck('id')->toArray();

            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60*20);

            $notificationBuilder = new PayloadNotificationBuilder('¡Su Orden ha llegado!');
            $notificationBuilder->setBody('Por favor acerquese al repartidor para recibirlo. Asegurese de revisarlo y de tener el dinero completo a la mano. ¡Gracias por comprar con PRONTO!')
                                ->setSound('default');

            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData([
                'arrived' => true,
                'pedido' => $this->getOrdenById($request->ordenId)
            ]);

            $option = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();

            // echo $user->firebase_token;
            $userTokens=FirebaseToken::whereIn('user_id',$user)->pluck('firebase_token')->toArray();

             if(count($userTokens)>0){

                // $token = $user->firebase_token;

                $downstreamResponse = FCM::sendTo($userTokens, $option, $notification, $data);
                $downstreamResponse->numberSuccess();
                $downstreamResponse->numberFailure();
                $downstreamResponse->numberModification();
            }
            
            
        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function deliveryDone(Request $request){
        try {
            $ord=Orden::where('id',$request->ordenId)->first();
            $ord->estado="completo";
            $ord->update();

            // $user=User::where('id',1)->first();
            $users = DB::table('users')
            ->join('model_has_roles', 'users.id', 'model_has_roles.model_id')
            ->select( 
                'users.id as id'
                )
            ->where('model_has_roles.role_id', 1)
            ->orWhere('model_has_roles.role_id', 5)
            ->pluck('id')->toArray();

            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60*20);

            $notificationBuilder = new PayloadNotificationBuilder('Orden completada');
            $notificationBuilder->setBody('')
                                ->setSound('default');

            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData(['a_data' => 'my_data']);

            $option = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();

            // echo $user->firebase_token;
            $userTokens=FirebaseToken::whereIn('user_id',$users)->pluck('firebase_token')->toArray();

            if(count($userTokens)>0){

                // $token = $user->firebase_token;

                $downstreamResponse = FCM::sendTo($userTokens, $option, $notification, $data);
                $downstreamResponse->numberSuccess();
                $downstreamResponse->numberFailure();
                $downstreamResponse->numberModification();
            }

            $pedidos=Pedido::where('orden_id',$request->ordenId)->get();

            foreach($pedidos as $ped){
                $ped->estado="completo";
                $ped->update();

                // $user=User::where('id',$ped->establecimiento_id)->first();

                $users=User::where('establecimiento_id',$ped->establecimiento_id)->pluck('id')->toArray();

                $optionBuilder = new OptionsBuilder();
                $optionBuilder->setTimeToLive(60*20);

                $notificationBuilder = new PayloadNotificationBuilder('Orden Completada');
                $notificationBuilder->setBody('')
                                    ->setSound('default');

                $dataBuilder = new PayloadDataBuilder();
                $dataBuilder->addData(['a_data' => 'my_data']);

                $option = $optionBuilder->build();
                $notification = $notificationBuilder->build();
                $data = $dataBuilder->build();

                $userTokens=FirebaseToken::whereIn('user_id',$users)->pluck('firebase_token')->toArray();


                // echo $user->firebase_token;
                if(count($userTokens)>0){

                    // $token = $user->firebase_token;

                    $downstreamResponse = FCM::sendTo($userTokens, $option, $notification, $data);
                    $downstreamResponse->numberSuccess();
                    $downstreamResponse->numberFailure();
                    $downstreamResponse->numberModification();
                }
            }

            
            
            
        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }


    public function getOrdenesCompletasByRepartidor($repId){
        
        try {
            $myArray = []; 

            foreach(Orden::where('repartidor_id', $repId)->Where('ordens.estado','completo')->get() as $ord)
            {
                $myArray2=[];

                $pedidos = DB::table('ordens')
                ->join('pedidos', 'ordens.id', 'pedidos.orden_id')
                ->join('establecimientos', 'establecimientos.id', 'pedidos.establecimiento_id')
                ->select( 
                    'pedidos.id',
                    'pedidos.subtotal',
                    'pedidos.total',
                    'pedidos.iva',
                    'pedidos.estado',
                    'establecimientos.nombre AS establecimiento',
                    'establecimientos.direccion AS direccion',
                    'establecimientos.lat AS lat',
                    'establecimientos.lng AS lng',
                    'pedidos.establecimiento_id',
                    'pedidos.orden_id',
                    'pedidos.tiempo'

                    )
                ->where('ordens.id',$ord->id)
                ->Where('ordens.estado','completo')->get();

                foreach($pedidos as $p){

                    $sub=DB::table('pedidos')
                    ->join('pedido_detalles', 'pedidos.id', 'pedido_detalles.pedido_id')
                    ->join('productos', 'productos.id', 'pedido_detalles.producto_id')
                    ->join('establecimientos', 'establecimientos.id', 'productos.establecimiento_id')
                    ->join('categorias', 'categorias.id', 'establecimientos.categoria_id')
                    ->select( 
                        'productos.id',
                        'productos.nombre',
                        'productos.linkImagen',
                        'productos.descripcion',
                        'productos.precio',
                        'productos.disponible',
                        'categorias.id AS categoria_id',
                        'categorias.nombre AS categoria',
                        'pedido_detalles.id AS pedido_id',
                        'pedido_detalles.cant AS cant' 
                    )
                    ->where('pedidos.orden_id',$ord->id)
                    ->where('pedidos.establecimiento_id',$p->establecimiento_id)->get();

                    $arrayTemp=array(
                        'id'=>$p->id,
                        'subtotal'=>$p->subtotal,
                        'total'=>$p->total,
                        'iva'=>$p->iva,
                        'estado'=>$p->estado,
                        'establecimiento'=>$p->establecimiento,
                        'establecimiento_id'=>$p->establecimiento_id,
                        'direccion_estab'=>$p->direccion,
                        'lat'=>$p->lat,
                        'lng'=>$p->lng,
                        'orden_id'=>$p->orden_id,
                        'tiempo'=>$p->tiempo,
                        'productos'=>$sub
                    );

                    array_push($myArray2, $arrayTemp);

                }

                $dir=DB::table('ordens')
                ->join('direccions','direccions.id','ordens.direccion_id')
                ->select( 
                    'direccions.direccion',
                    'direccions.lat',
                    'direccions.lng',
                    'direccions.id'
                )
                ->where('ordens.id',$ord->id)
                ->first();

                $ordTemp=array(
                    'total'=>$ord->total,
                    'user'=> User::select('name')->where('id',$ord->user_id)->first(),
                    'direccion'=>$dir,
                    'cant'=>$ord->cant,
                    'orden_id'=>$ord->id,
                    'estate'=>$ord->estado,
                    'orden_codigo'=>$ord->codigo,
                    'pedidos'=>$myArray2,
                    'tiempoEstabs'=>$ord->tiempoEstabs,
                    'costo_delivery'=>$ord->costo_delivery,
                    'tiempo'=>$ord->tiempo,
                    'subtotal'=>$ord->subtotal,
                    'tiempoDel'=>$ord->tiempoDel
                );

                array_push($myArray, $ordTemp);

            }

            return $myArray;
        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
    
}
