<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->longText('descripcion')->nullable();
            $table->text('linkImagen')->nullable();
            // $table->bigInteger('categoria_id')->unsigned();
            $table->bigInteger('establecimiento_id')->unsigned()->nullable();
            $table->float('precio');
            $table->boolean('disponible');
            $table->timestamps();

            $table->foreign('establecimiento_id')->references('id')->on('establecimientos')->onDelete('cascade');
            // $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
