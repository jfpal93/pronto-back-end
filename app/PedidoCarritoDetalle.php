<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PedidoCarritoDetalle extends Model
{
    protected $table = 'pedido_carrito_detalles';
    protected $fillable = ['pedido_carrito_id', 'producto_id','cant'];

    public function pedido_carrito()
	{
		return $this->belongsTo('App\PedidoCarrito');
    }

    public function producto()
	{
		return $this->belongsTo('App\Producto');
    }

}
