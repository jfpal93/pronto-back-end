<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSugerenciaDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sugerencia_detalles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sugerencia_id')->unsigned();
            $table->bigInteger('producto_id')->unsigned();
            $table->timestamps();

            $table->foreign('sugerencia_id')->references('id')->on('sugerencias')->onDelete('cascade');
            $table->foreign('producto_id')->references('id')->on('productos')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sugerencia_detalles');
    }
}
